<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('front.index');
Route::get('help', 'FrontController@help')->name('front.help');
Route::get('contact', 'FrontController@contact')->name('front.contact');
Route::get('info', 'FrontController@info')->name('front.info');
Route::get('terms', 'FrontController@terms')->name('front.terms');
Route::post('/contact/send', 'FrontController@contactSend')->name('contact.send');



Route::get('refresh-csrf', function(){
    return csrf_token();
});

Route::get('refresh/{code}', function($code){
      
      if($code=='system6OW1ovwmUrQf5kfeyalegria'){
      
          Artisan::call('migrate:rollback');
    
    	  $exitCode = Artisan::call('migrate:refresh', [
    	    '--force' => true,
    	  ]);
    	  
    	  Artisan::call('db:seed');
    
    	  return 'Code: '.$exitCode;
    	  
     }else{
         return "fallo";
     }

});

/*Route::get('pdftickets', function(){
	$data=\App\Inscription::with(['competitors' => function ($query) {  
        $query->with(['size' => function ($query) {          
            }]);         
        }])->with(['payments' => function ($query) {
            $query->with(['bank' => function ($query) {          
            }]);          
        }])->findOrFail(1);

    return view('app.report.pdftickets',compact('data'));
});*/

Route::get('inscription', 'FrontInscriptionController@index')->name('front.inscription');
Route::post('inscription/store', 'FrontInscriptionController@store')->name('front.inscription.store');
Route::get('inscription/show/{id}', 'FrontInscriptionController@show')->name('front.inscription.show');
Route::get('inscription/ticket/{code}', 'FrontInscriptionController@ticket')->name('front.inscription.ticket');

/* Init Auth */
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

/* End Auth */

Route::middleware(['auth'])->group(function () {

 Route::middleware(['type:master'])->group(function () {

  Route::get('dashboard/cache', function(){
	Artisan::call('cache:clear');
	Artisan::call('config:clear');
	Artisan::call('route:clear');
	Artisan::call('view:clear');
	return "Bien Clear";
  });
  

 });

 Route::middleware(['type:master|manager|operator'])->group(function () {
 
 	Route::get('/home', 'HomeController@index')->name('home');

 	Route::get('/dashboard/inscription', 'InscriptionController@index')
	 ->name('inscription');
	 Route::get('/dashboard/inscription/data', 'InscriptionController@data')
	 ->name('inscription.data');
	 Route::get('/dashboard/inscription/create', 'InscriptionController@create')
	 ->name('inscription.create');
	 Route::post('/dashboard/inscription/store', 'InscriptionController@store')
	 ->name('inscription.store');
	 Route::get('/dashboard/inscription/show/{id}', 'InscriptionController@show')
	 ->name('inscription.show');
	 Route::get('/dashboard/inscription/ticket/{id}', 'ReportController@pdfticket')
	 ->name('inscription.ticket');	
	 Route::put('/dashboard/inscription/comment/{id}', 'InscriptionController@comment')->name('inscription.comment');
	 Route::put('/dashboard/inscription/sendcomment/{id}', 'InscriptionController@sendcomment')->name('inscription.sendcomment');

	 Route::get('/dashboard/profile', 'ProfileController@index')->name('profile');
     //Route::put('/dashboard/profile/update', 'ProfileController@update')->name('profile.update');
 	 Route::put('/dashboard/profile/updatePassword', 'ProfileController@updatePassword')->name('profile.update.password');

 	 Route::get('/dashboard/inscription/{id}/status-rejected', 'InscriptionController@rejected')
	 ->name('inscription.rejected');
	 Route::get('/dashboard/inscription/{id}/status-process', 'InscriptionController@process')
	 ->name('inscription.process');
	 Route::get('/dashboard/inscription/{id}/status-approved', 'InscriptionController@approved')
	 ->name('inscription.approved');

	 Route::get('/dashboard/inscription/{id}/remove', 'InscriptionController@remove')
	 ->name('inscription.remove'); 

	 Route::get('/dashboard/inscription/{id}/kits-yes', 'InscriptionController@kitsOK')
	 ->name('inscription.kitsOK'); 

	 Route::get('/dashboard/inscription/{id}/kits-no', 'InscriptionController@kitsNO')
	 ->name('inscription.kitsNO'); 

 });

 Route::middleware(['type:master|manager'])->group(function () {


 	Route::get('/dashboard/page/edit/{page}', 'PageController@edit')->name('page');
    Route::put('/dashboard/page/update/{id}', 'PageController@update')->name('page.update');
    Route::get('/dashboard/user', 'UserController@index')->name('user');
 	Route::get('/dashboard/user/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
 	Route::post('/dashboard/user/register', 'Auth\RegisterController@register');

 	Route::get('/dashboard/user/disable/{id}',
	    [
	      'as' =>'user.disable',
	      'uses' =>'UserController@disable'
	    ]
	 );
	 Route::get('/dashboard/user/active/{id}',
	    [
	      'as' =>'user.active',
	      'uses' =>'UserController@active'
	    ]
	 );

	 Route::get('/dashboard/bank', 'BankController@index')->name('bank');
	 Route::get('/dashboard/bank/create', 'BankController@create')->name('bank.create');
	 Route::post('/dashboard/bank/store', 'BankController@store')->name('bank.store');
	 Route::get('/dashboard/bank/edit/{id}', 'BankController@edit')->name('bank.edit');
	 Route::put('/dashboard/bank/update/{id}', 'BankController@update')->name('bank.update');
	 Route::get('/dashboard/bank/remove/{id}', 'BankController@remove')->name('bank.remove');

	 Route::get('/dashboard/size', 'SizeController@index')->name('size');
	 Route::get('/dashboard/size/create', 'SizeController@create')->name('size.create');
	 Route::post('/dashboard/size/store', 'SizeController@store')->name('size.store');
	 Route::get('/dashboard/size/edit/{id}', 'SizeController@edit')->name('size.edit');
	 Route::put('/dashboard/size/update/{id}', 'SizeController@update')->name('size.update');
	 Route::get('/dashboard/size/remove/{id}', 'SizeController@remove')->name('size.remove');

	 Route::get('/dashboard/sponsor', 'SponsorController@index')->name('sponsor');
	 Route::get('/dashboard/sponsor/create', 'SponsorController@create')->name('sponsor.create');
	 Route::post('/dashboard/sponsor/store', 'SponsorController@store')->name('sponsor.store');
	 Route::get('/dashboard/sponsor/edit/{id}', 'SponsorController@edit')->name('sponsor.edit');
	 Route::put('/dashboard/sponsor/update/{id}', 'SponsorController@update')->name('sponsor.update');
	 Route::get('/dashboard/sponsor/remove/{id}', 'SponsorController@remove')->name('sponsor.remove');


	 Route::get('/dashboard/slider', 'SliderController@index')->name('slider');
	 Route::get('/dashboard/slider/create', 'SliderController@create')->name('slider.create');
	 Route::post('/dashboard/slider/store', 'SliderController@store')->name('slider.store');
	 Route::get('/dashboard/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
	 Route::put('/dashboard/slider/update/{id}', 'SliderController@update')->name('slider.update');
	 Route::get('/dashboard/slider/remove/{id}', 'SliderController@remove')->name('slider.remove');
	 
	 Route::get('/dashboard/report', 'ReportController@index')->name('report');
	 Route::post('/dashboard/report', 'ReportController@search')->name('report.search');
	 

  });

 
 /*Route::get('/dashboard/profile', 'ProfileController@index')->name('profile');
 Route::put('/dashboard/profile/update', 'ProfileController@update')->name('profile.update');
 Route::put('/dashboard/profile/updatePassword', 'ProfileController@updatePassword')->name('profile.update.password');*/ 
 
});

