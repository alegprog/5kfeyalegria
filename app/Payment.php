<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
	  use SoftDeletes;

    protected $appends = ['method'];

    protected $dates = ['deleted_at'];

    public function inscription()
    {
          return $this->belongsTo('App\Inscription');
    }

    public function bank()
    {
          return $this->belongsTo('App\Bank');
    }

    public function getMethodAttribute(){
      
      $method=$this->payment_method;
      if($method=='cash'){
        return 'Efectivo';
      }elseif($method=='transfer'){
        return 'Transferencia';
      }elseif($method=='deposit'){
        return 'Deposito';
      }elseif($method=='office'){
        return 'Pago en oficina';
      }else{
        return '';
      }

    }
}
