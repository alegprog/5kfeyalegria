<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inscription extends Model
{
    use SoftDeletes;

    protected $appends = ['created_at_es','type_es','status_es','registered_by'];

    protected $dates = ['deleted_at'];

    public function user()
    {
          return $this->belongsTo('App\User');
    }

    public function competitors()
    {
          return $this->hasMany('App\Competitor');
    }


    public function payments()
    {
          return $this->hasMany('App\Payment');
    }

    public function getCreatedAtEsAttribute(){

     return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y H:i:s');

    }

    public function getTypeEsAttribute(){
      if($this->type=='online'){
        return 'Online';
      }elseif($this->type=='office'){
        return 'Oficina';
      }
    }

    public function getStatusEsAttribute(){
      if($this->status=='pending'){
        return 'Pendiente';
      }elseif($this->status=='approved'){
        return 'Aprobado';
      }elseif($this->status=='rejected'){
        return 'Rechazado';
      }elseif($this->status=='process'){
        return 'En proceso';
      }
    }

    public function getRegisteredByAttribute(){
      return $this->user_id ? $this->user->name : 'Web';
    }

   

}
