<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Inscription;

class SendInscription extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $inscription;
    public $actionUrl;

    public function __construct(Inscription $inscription)
    {
        $this->inscription = $inscription;
        $this->actionUrl = route('front.inscription.show',$inscription->code);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      
      return $this->markdown('emails.inscription.register')
      ->subject(trans('Registro de inscripción carrera 5k por la educación - '.$this->inscription->created_at));
    }
}