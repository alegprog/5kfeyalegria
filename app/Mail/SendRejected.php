<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Inscription;

class SendRejected extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $inscription;
    public $actionUrl;
    public $urlHelp;
    public $urlContact;

    public function __construct(Inscription $inscription)
    {
        $this->inscription = $inscription;
        $this->actionUrl = route('front.inscription.show',$inscription->code);
        $this->urlHelp = route('front.help');
        $this->urlContact = route('front.contact');

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      
      return $this->markdown('emails.inscription.rejected')
      ->subject(trans('Inscripción RECHAZADA carrera 5k por la educación - '.$this->inscription->updated_at));
    }
}