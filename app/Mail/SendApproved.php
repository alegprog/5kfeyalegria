<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Inscription;
use PDF;

class SendApproved extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $inscription;
    public $actionUrl;
    public $urlHelp;
    public $urlContact;

    public function __construct(Inscription $inscription)
    {
        $this->inscription = $inscription;
        $this->actionUrl = route('front.inscription.show',$inscription->code);
        $this->urlHelp = route('front.help');
        $this->urlContact = route('front.contact');

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

      $data=$this->inscription;

      $pdf = PDF::loadview('app.report.pdftickets', ['data'=>$data]);
      
      return $this->markdown('emails.inscription.approved')
      ->subject(trans('Inscripción APROBADA carrera 5k por la educación - '.$this->inscription->updated_at))
      ->attachData($pdf->output(), 'ticket-'.str_pad($this->inscription->id,7,"0",STR_PAD_LEFT).'.pdf', ['mime' => 'application/pdf',
                    ]);
    }
}