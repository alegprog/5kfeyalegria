<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Competitor extends Model
{
	use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = ['birthdate_es','age','category_type'];

    public function inscription()
    {
          return $this->belongsTo('App\Inscription');
    }

    public function size()
    {
          return $this->belongsTo('App\Size');
    }

    public function getBirthdateEsAttribute(){

     return \Carbon\Carbon::createFromFormat('Y-m-d', $this->birthdate)->format('d/m/Y');

    }

    public function getAgeAttribute(){

    	return \Carbon\Carbon::createFromFormat('Y-m-d', $this->birthdate)->age;
    	//return '';

    }

    public function getCategoryTypeAttribute(){
        if($this->age <=19){
            return 'Junior';
        }elseif($this->age <= 29){
            return 'Master 20';
        }elseif($this->age <= 39){
            return 'Master 30';
        }elseif($this->age <= 49){
            return 'Master 40';
        }elseif($this->age <= 59){
            return 'Master 50';
        }elseif($this->age <= 69){
            return 'Master 60';
        }elseif($this->age >= 70){
            return 'Master 70';
        }
    }
}
