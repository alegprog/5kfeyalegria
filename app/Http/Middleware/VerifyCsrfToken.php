<?php

namespace App\Http\Middleware;
use Closure;
use Session;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
      if($request->input('_token'))
      {
        $token=session('_token');
        //dd($a);
        if ( $token != $request->input('_token'))
        {
          return redirect()->guest('/')
          ->with('global', 'Fue redirecionado porque ha expirado la sesión');
        }
      }
      return parent::handle($request, $next);
    }
}
