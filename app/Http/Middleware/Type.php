<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Type
{
  /**
   * The Guard implementation.
   *
   * @var Guard
   */
  protected $auth;

  /**
   * Create a new filter instance.
   *
   * @param  Guard  $auth
   * @return void
   */
  public function __construct(Guard $auth)
  {
      $this->auth = $auth;
  }
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next,$type)
  {
      $types=explode('|',$type);
      $status=0;
      foreach($types as $usertype){
        if ($this->auth->user()->type == $usertype) {
          return $next($request);
        }
      }

      if($status==0){
        if ($request->ajax()) {
            return response('Unauthorized.', 401);
        } else {
            return redirect()->to('home');
        }
      }


  }
}
