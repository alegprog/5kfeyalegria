<?php

namespace App\Http\Requests\Inscription;

use Illuminate\Foundation\Http\FormRequest;

class StoreInscription extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function all()
    {
        $input = parent::all();  

        /*if(empty($input['birthdate'])){

        }else{  



            $input['birthdate'] = \Carbon\Carbon::createFromFormat('d/m/Y', $input['birthdate'])->toDateString();
        }*/

        //dd($input['birthdate']); 

        /*$input['competitors.*.size'] = str_replace(',','.',$this->input('competitors.*.size'));*/
               

        return $input;
    }


    public function rules()
    {
        return [
            /*'name'=>'required',
            'identification'=>'required',
            'email'=>'required|email',
            'gender'=>'required',
            'size'=>'required',
            'birthdate'=>'required|date_format:"d-m-Y"|before:"2010-01-01"',*/
            /*'payment_method'=>'required',
            'amount'=>'required|numeric|min:1',
            'number'=>'required|digits_between:4,15',
            'bank'=>'required|exists:banks,id',*/
            'competitors'=>'present|array',
            'competitors.*.name'=>'required|string',
            'competitors.*.identification'=>'required|unique:competitors|distinct',
            'competitors.*.email'=>'required|email|unique:competitors|distinct',
            'competitors.*.gender'=>'required|in:M,F',
            'competitors.*.size'=>'required|exists:sizes,id',
            'competitors.*.birthdate'=>'required|date_format:"d-m-Y"',
            'payments'=>'present|array',
            'payments.*.payment_method'=>'required|in:cash,transfer,deposit',
            //'payments.*.amount'=>'required|numeric|min:1',
            'payments.*.number'=>'nullable|required_if:payments.*.payment_method,transfer|required_if:payments.*.payment_method,deposit|digits_between:4,15|unique:payments|distinct',
            'payments.*.bank'=>'nullable|required_if:payments.*.payment_method,transfer|required_if:payments.*.payment_method,deposit|exists:banks,id',
        ];
    }
}
