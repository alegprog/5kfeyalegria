<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id=auth()->user()->id;
        return [
          'name' => 'required|string|max:255',
          'email' => 'required|email|max:255|unique:users,email,'.$user_id.',id',
        ];
    }
}
