<?php

namespace App\Http\Requests\Report;

use Illuminate\Foundation\Http\FormRequest;

class SearchReport extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function all()
    {
        $input = parent::all();  

        /*if(empty($input['birthdate'])){

        }else{  



            $input['birthdate'] = \Carbon\Carbon::createFromFormat('d/m/Y', $input['birthdate'])->toDateString();
        }*/

        //dd($input['birthdate']); 

        /*$input['competitors.*.size'] = str_replace(',','.',$this->input('competitors.*.size'));*/
               

        return $input;
    }


    public function rules()
    {
        return [            
            'date_init'=>'required|date_format:"d-m-Y"',
            'date_end'=>'required|date_format:"d-m-Y"',
        ];
    }
}
