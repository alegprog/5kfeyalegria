<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Inscription\StoreFrontInscription;
use App\Mail\SendInscription as SendInscriptionMail;
use App\Inscription;
use App\Bank;
use App\Competitor;
use App\Payment;
use App\Sponsor;
use App\Size;
use Alert;
use Mail;
use PDF;


class FrontInscriptionController extends Controller
{
    public function index(){
        $sponsors = Sponsor::get();
        $sizes= Size::all();
    	$banks= Bank::all();
    	$payment_method=[    		
    		['id'=>'transfer','name'=>'Transferencia'],
    		['id'=>'deposit','name'=>'Depósito'],
            ['id'=>'office','name'=>'Pago en oficina'],
    	];

    	//dd($payment_method);
    	return view('front.inscription.index',compact('sizes','banks','payment_method','sponsors'));
    }

    public function store(StoreFrontInscription $request){



        $inscription = New Inscription();
        $inscription->user_id=null;
        $inscription->type='online';
        $inscription->status='pending';
        $inscription->comment='';

        if(count($request->competitors)<1){
            return redirect()->route('front.inscription')
                        ->withInput();
        }
        //dd($request->competitors[1]['size']);
        $size=$request->competitors[1]['size'];
        /*$query_size=Competitor::withCount(['size' => function ($query) use($size) {
            $query->where('id',$size);
        }])->where('status', '<>', 'rejected')->get();*/

        $query_size=Competitor::where('size_id',$size)->get();
        $size_name=Size::findOrFail($size);

        

        if($size_name->name=='S'){
           if($query_size->count()>=1){
                Alert()->warning('La talla selecionada ya no se encuentra disponible por favor elija otra','! Advertencia ¡')->persistent("Cerrar");
                return redirect()->route('front.inscription')
                ->withInput();
           }                       
        }elseif($size_name->name=='M'){
            if($query_size->count()>=1){
                Alert()->warning('La talla selecionada ya no se encuentra disponible por favor elija otra','! Advertencia ¡')->persistent("Cerrar");
                return redirect()->route('front.inscription')
                ->withInput();
            }  
        }elseif($size_name->name=='L'){
            if($query_size->count()>=1){
                Alert()->warning('La talla selecionada ya no se encuentra disponible por favor elija otra','! Advertencia ¡')->persistent("Cerrar");
                return redirect()->route('front.inscription')
                ->withInput();
           }
        }elseif($size_name->name=='XL'){
            if($query_size->count()>=1){
                Alert()->warning('La talla selecionada ya no se encuentra disponible por favor elija otra','! Advertencia ¡')->persistent("Cerrar");
                return redirect()->route('front.inscription')
                ->withInput();
           }
        }

        //dd($query_size->count(),$size,$size_name);

        if(count($request->payments)<1){
            return redirect()->route('front.inscription')
                        ->withInput();
        }

        if($inscription->save()){
            $inscription->code=strtoupper(str_random(10)).'-'.$inscription->id;
            $inscription->save();

            if($request->competitors){
                foreach ($request->competitors as $value) {

                    $competitor = new Competitor();
                    $competitor->identification =$value['identification'];
                    $competitor->name =$value['name'];
                    $competitor->email =$value['email'];
                    $competitor->gender =$value['gender'];
                    $competitor->size_id =$value['size'];
                    $competitor->birthdate =\Carbon\Carbon::createFromFormat('d-m-Y', $value['birthdate'])->toDateString();
                    $competitor->category=NULL;
                    $competitor->inscription_id =$inscription->id;
                    $competitor->save();

                    $competitor->category=$this->getCategoryType($competitor->birthdate,$competitor->gender);
                    $competitor->save();

                }
            }

            if($request->payments){
                foreach ($request->payments as $value) {

                    $payment = new Payment();
                    $payment->payment_method=$value['payment_method'];
                    $payment->amount=isset($value['amount']) ? $value['amount'] : NULL;
                    $payment->number=isset($value['number']) ? $value['number'] : NULL;
                    $payment->bank_id=isset($value['bank']) ? $value['bank'] : NULL;
                    $payment->inscription_id=$inscription->id;
                    $payment->save();

                }
            }


            //dd($inscription);
        }else{
            return redirect()->back()->with('danger', 'Ha ocurrido un error con el servidor por favor intente nuevamente');
        }  

        foreach ($request->competitors as $value) {
            Mail::to($value['email'],'Registro carrera 5K por la educación')              
            ->send(new SendInscriptionMail($inscription));
        }


        Alert()->success('Se ha procesado su solicitud exitosamente, en las proximas horas sera verificada su inscripción','! Excelente ¡')->autoclose(6000);

        return redirect()->route('front.inscription.show',$inscription->code)->with('success', trans('message.store'));      

    }

    public function show($code){

        $sponsors = Sponsor::get();

        $inscription = Inscription::with(['competitors' => function ($query) {          
        }])->with(['payments' => function ($query) {
            $query->with(['bank' => function ($query) {          
            }]);          
        }])->where('code',$code)->first();

        if(!$inscription){
            abort(404);
        }

        //dd($inscription);

        return view('front.inscription.show',compact('inscription','sponsors'));
    }

    public function ticket($code){
      $inscription = Inscription::with(['competitors' => function ($query) {  
        $query->with(['size' => function ($query) {          
            }]);         
        }])->with(['payments' => function ($query) {
            $query->with(['bank' => function ($query) {          
            }]);          
        }])->where('code',$code)->first();

        if(!$inscription){
            abort(404);
        }

        $pdf = PDF::loadview('app.report.pdftickets', ['data'=>$inscription]);

        return $pdf->stream('ticket-'.str_pad($inscription->id,7,"0",STR_PAD_LEFT).'.pdf');
    }

    public function getCategoryType($birthdate,$gender){
        $age=\Carbon\Carbon::createFromFormat('Y-m-d', $birthdate)->age;
        if($age <=18 && $gender == 'M'){
            return 'Infantil Masculino';
        }elseif($age <=18 && $gender == 'F'){
            return 'Infantil Femenino';
        }elseif($age <= 30 && $gender == 'M'){
            return 'Juvenil Masculino';
        }elseif($age <= 30 && $gender == 'F'){
            return 'Juvenil Femenino';
        }elseif($age >= 31 && $gender == 'M'){
            return 'Adulto Masculino';
        }elseif($age >= 31 && $gender == 'F'){
            return 'Adulto Femenino';
        }
    }
}
