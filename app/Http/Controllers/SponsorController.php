<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Sponsor\StoreSponsor;
use App\Http\Requests\Sponsor\UpdateSponsor;
use App\Sponsor;
use Storage;

class SponsorController extends Controller
{
  public function index()
	{
	    $sponsors=Sponsor::paginate(10);
	    return view('app.sponsor.index',compact('sponsors'));
	}

	public function create()
	{
	    return view('app.sponsor.create');
	}

	public function store(StoreSponsor $request)
	{
      $sponsor = new Sponsor();
      $sponsor->name=$request->name;
      $sponsor->logo='';      
      if($sponsor->save()){
       if ($request->hasFile('logo')){
         $path = $request->file('logo')->store('sponsor'.'/'.$sponsor->id,'sponsor');
         $sponsor->logo=$path;
         $sponsor->save();
       }
     }

      return redirect()->route('sponsor')->with('success', trans('message.store'));

	}

	public function edit($id){
    	$sponsor=Sponsor::findOrFail($id);
    	return view('app.sponsor.edit', compact('sponsor'));
    }

    public function Update(UpdateSponsor $request,$id)
   {
       $sponsor = Sponsor::findOrFail($id);
       $sponsor->name=$request->name;    
       if($sponsor->save()){
        if ($request->hasFile('logo')){
           $logo =$sponsor->logo;
           if(Storage::disk('sponsor')->has($logo)){
             Storage::disk('sponsor')->delete($logo);
           }
           $path = $request->file('logo')->store('sponsor'.'/'.$sponsor->id,'sponsor');
           $sponsor->logo=$path;
           $sponsor->save();
         }
      }

       return redirect()->route('sponsor')->with('success', trans('message.update'));

   }

   public function remove($id){

        $sponsor=Sponsor::withTrashed()->findOrFail($id);   
        $sponsor->delete();   

        return redirect()->route('sponsor')->with('success', trans('message.delete'));

    }
}
