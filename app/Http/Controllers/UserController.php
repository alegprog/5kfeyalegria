<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index(){
    	$users= User::withTrashed()->where('type', '<>', 'master')->paginate(20);    	

    	return view('app.user.index',compact('users'));
    }

   public function disable(Request $request, $id)
   {
       $user= User::withTrashed()->findOrFail($id);

       $user->delete();
       
       return redirect()->back()->with('success',trans('message.disable'));
   }

   public function active(Request $request, $id)
   {
       $schedule= User::withTrashed()->findOrFail($id);

       $schedule->restore();
       
       return redirect()->back()->with('success',trans('message.active'));
   }


}
