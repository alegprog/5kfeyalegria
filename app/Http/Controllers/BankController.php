<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;
use App\Http\Requests\Bank\StoreBank;
use App\Http\Requests\Bank\UpdateBank;

class BankController extends Controller
{
  public function index()
  {
      $banks=Bank::paginate(10);
      return view('app.bank.index',compact('banks'));
  }

  public function create()
  {
      return view('app.bank.create');
  }

  public function store(StoreBank $request)
  {
      $bank = new Bank();
      $bank->name=$request->name;      
      $bank->save();

      return redirect()->route('bank')->with('success', trans('message.store'));

  }

  public function edit($id){
    $bank=Bank::findOrFail($id);
    return view('app.bank.edit', compact('bank'));
  }

  public function update(UpdateBank $request, $id)
  {
      $bank = Bank::findOrFail($id);
      $bank->name=$request->name;
      $bank->save();

      return redirect()->route('bank')->with('success', trans('message.update'));

  }

  public function remove($id){

        $bank=Bank::withTrashed()->findOrFail($id);   
        $bank->delete();   

        return redirect()->route('bank')->with('success', trans('message.delete'));

    }
}
