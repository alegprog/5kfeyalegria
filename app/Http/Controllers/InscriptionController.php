<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Inscription\StoreInscription;
use App\Http\Requests\Inscription\UpdateComment;
use App\Mail\SendInscription as SendInscriptionMail;
use App\Mail\SendRejected as SendRejectedMail;
use App\Mail\SendApproved as SendApprovedMail;
use App\Mail\SendComment as SendCommentMail;
use App\Inscription;
use App\Bank;
use App\Competitor;
use App\Payment;
use App\Size;
use Alert;
use Mail;
use Yajra\Datatables\Datatables;

class InscriptionController extends Controller
{
    public function index(){
        
    	$inscriptions= Inscription::orderBy('created_at','desc')->paginate(20);
    	return view('app.inscription.index',compact('inscriptions'));
    }

    public function create(){
        $sizes= Size::all();
    	$banks= Bank::all();

        if(auth()->user()->type=='manager' || auth()->user()->type=='master'){
        	$payment_method=[
        		['id'=>'cash','name'=>'Efectivo'],
        		['id'=>'transfer','name'=>'Transferencia'],
        		['id'=>'deposit','name'=>'Depósito']
        	];
        }elseif(auth()->user()->type=='operator'){
            $payment_method=[
                ['id'=>'cash','name'=>'Efectivo']
            ];
        }else{
            $payment_method=[];
        }

    	//dd($payment_method);
    	return view('app.inscription.create',compact('sizes','banks','payment_method'));
    }

    public function store(StoreInscription $request){

        $inscription = New Inscription();
        $inscription->user_id=auth()->user()->id;
        $inscription->type='office';      
        $inscription->status='pending';
        $inscription->comment='';

        if(count($request->competitors)<1){
            return redirect()->route('inscription.create')
                        ->withInput();
        }

        $size=$request->competitors[1]['size'];
        $query_size=Competitor::where('size_id',$size)->get();
        $size_name=Size::findOrFail($size);

        

        if($size_name->name=='S'){
           if($query_size->count()>=1){
                Alert()->warning('La talla selecionada ya no se encuentra disponible por favor elija otra','! Advertencia ¡')->persistent("Cerrar");
                return redirect()->route('inscription.create')
                ->withInput();
           }                       
        }elseif($size_name->name=='M'){
            if($query_size->count()>=1){
                Alert()->warning('La talla selecionada ya no se encuentra disponible por favor elija otra','! Advertencia ¡')->persistent("Cerrar");
                return redirect()->route('inscription.create')
                ->withInput();
            }  
        }elseif($size_name->name=='L'){
            if($query_size->count()>=1){
                Alert()->warning('La talla selecionada ya no se encuentra disponible por favor elija otra','! Advertencia ¡')->persistent("Cerrar");
                return redirect()->route('inscription.create')
                ->withInput();
           }
        }elseif($size_name->name=='XL'){
            if($query_size->count()>=1){
                Alert()->warning('La talla selecionada ya no se encuentra disponible por favor elija otra','! Advertencia ¡')->persistent("Cerrar");
                return redirect()->route('inscription.create')
                ->withInput();
           }
        }

        if(count($request->payments)<1){
            return redirect()->route('inscription.create')
                        ->withInput();
        }

        if($inscription->save()){
            $inscription->code=strtoupper(str_random(10)).'-'.$inscription->id;
            $inscription->save();

            if($request->competitors){
                foreach ($request->competitors as $value) {

                    $competitor = new Competitor();
                    $competitor->identification =$value['identification'];
                    $competitor->name =$value['name'];
                    $competitor->email =$value['email'];
                    $competitor->gender =$value['gender'];
                    $competitor->size_id =$value['size'];
                    $competitor->birthdate =\Carbon\Carbon::createFromFormat('d-m-Y', $value['birthdate'])->toDateString();
                    $competitor->category=NULL;
                    $competitor->inscription_id =$inscription->id;
                    $competitor->save();

                    $competitor->category=$this->getCategoryType($competitor->birthdate,$competitor->gender);
                    $competitor->save();

                }
            }

            if($request->payments){
                foreach ($request->payments as $value) {

                    $payment = new Payment();
                    $payment->payment_method=$value['payment_method'];
                    $payment->amount=isset($value['amount']) ? $value['amount'] : NULL;
                    $payment->number=isset($value['number']) ? $value['number'] : NULL;
                    $payment->bank_id=isset($value['bank']) ? $value['bank'] : NULL;
                    $payment->inscription_id=$inscription->id;
                    $payment->save();

                }
            }

            foreach ($request->competitors as $value) {
                Mail::to($value['email'],'Registro carrera 5K por la educación')  
                ->send(new SendInscriptionMail($inscription));
            }


            //dd($inscription);
        }else{
            return redirect()->back()->with('danger', 'Ha ocurrido un error con el servidor por favor intente nuevamente');
        }  

        return redirect()->route('inscription')->with('success', trans('message.store'));      

    }

    public function show($id){

        $inscription = Inscription::with(['competitors' => function ($query) {  
            $query->with(['size' => function ($query) {          
            }]);        
        }])->with(['payments' => function ($query) {
            $query->with(['bank' => function ($query) {          
            }]);          
        }])->findOrFail($id);

        //dd($inscription);

        return view('app.inscription.show',compact('inscription'));
    }

    public function data(Datatables $datatables){

        $inscriptions = Inscription::with(['competitors' => function ($query) {          
        }])->with(['payments' => function ($query) {
            $query->with(['bank' => function ($query) {          
            }]);          
        }])->get();

        return $datatables::of($inscriptions)
        ->addColumn('action', function ($inscription) {
            return '<div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="fa fa-cog"></span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                      <li>
                        <a href="'.route('inscription.show',$inscription->id).'" >'.trans('action.show').'</a>
                      </li>
                    </ul>
                  </div>';           
        })->addColumn('zero_code', function ($inscription) {
            return str_pad($inscription->id,7,'0',STR_PAD_LEFT);  
        })->editColumn('created_at_es', function ($inscription) {
            $date=explode(' ',$inscription->created_at_es);
            return '<span class="text-center">'.$date[0].'</span><br><span>'.$date[1].'</span>';
        })->editColumn('status_es', function ($inscription) {
            if($inscription->status=='pending'){
                return '<span class="label label-warning f-14">'.$inscription->status_es.'</span>';
            }elseif($inscription->status=='approved'){
                return '<span class="label label-success f-14">'.$inscription->status_es.'</span>';
            }elseif($inscription->status=='rejected'){
                return '<span class="label label-danger f-14">'.$inscription->status_es.'</span>';
            }elseif($inscription->status=='process'){
                return '<span class="label label-info f-14">'.$inscription->status_es.'</span>';    
            }
        })->editColumn('type_es', function ($inscription) {
            if($inscription->type=='office'){
                return '<span class="f-14"><i class="text-warning fa fa-dot-circle-o"></i> '.$inscription->type_es.'</span>';
            }elseif($inscription->type=='online'){
                return '<span class="f-14"><i class="text-success fa fa-dot-circle-o"></i> '.$inscription->type_es.'</span>';
            }
        })->rawColumns(['action','status_es','type_es','created_at_es'])
        ->make(true);


    }


    public function rejected($id){

        $inscription = Inscription::with(['competitors' => function ($query) {   
        }])->findOrFail($id);
        $inscription->status='rejected';
        $inscription->save();

        foreach ($inscription->competitors as $competitor) {
            Mail::to($competitor->email,$competitor->name)  
            ->send(new SendRejectedMail($inscription));
        }

        Alert()->success('Se ha modificado correctamente el estatus a RECHAZADO','! Excelente ¡')->autoclose(4000);

        return redirect()->route('inscription.show',$id);
    }

    public function process($id){
        
        $inscription = Inscription::findOrFail($id);
        $inscription->status='process';
        $inscription->save();

        Alert()->success('Se ha modificado correctamente el estatus a EN PROCESO','! Excelente ¡')->autoclose(4000);

        return redirect()->route('inscription.show',$id);
    }

    public function approved($id){
        
        $inscription = Inscription::findOrFail($id);
        $inscription->status='approved';
        $competitor=$inscription->competitors->first();
        if($competitor->caregory==NULL){
           $competitor->category = $this->getCategoryType($competitor->birthdate,$competitor->gender);
           $competitor->save();
        }
        $inscription->save();

        foreach ($inscription->competitors as $competitor) {
            Mail::to($competitor->email,$competitor->name)  
            ->send(new SendApprovedMail($inscription));
        }

        Alert()->success('Se ha modificado correctamente el estatus a APROBADO','! Excelente ¡')->autoclose(4000);

        return redirect()->route('inscription.show',$id);
    }

    public function remove($id){

        $inscription = Inscription::findOrFail($id);

        //$competitor = Competitor::where('inscription_id',$id)->get();

        //$payment= Payment::where('inscription_id',$id)->get();

        $inscription->payments()->forceDelete();
        $inscription->competitors()->forceDelete();
        $inscription->forceDelete();

        return redirect()->route('inscription')->with('success', trans('message.delete'));  
    }

    public function kitsOK($id){
        $inscription = Inscription::findOrFail($id);
        $inscription->kits=1;
        $inscription->save();

        Alert()->success('Se ha RETIRADO el KITS correctamente','! Excelente ¡')->autoclose(4000);

        return redirect()->route('inscription.show',$id);
    }

    public function kitsNO($id){
        $inscription = Inscription::findOrFail($id);
        $inscription->kits=0;
        $inscription->save();

        Alert()->success('Se ha ANULADO el retiro el KITS correctamente','! Excelente ¡')->autoclose(4000);

        return redirect()->route('inscription.show',$id);
    }

    public function comment(UpdateComment $request, $id)
    {
          $inscription = Inscription::findOrFail($id);
          $inscription->comment=$request->comment;
          $inscription->save();

          Alert()->success('Se ha agregado un comentario de manera exitosa','! Excelente ¡')->autoclose(4000);

          return redirect()->route('inscription.show',$id);

    }

    public function sendComment(UpdateComment $request, $id)
    {
          $inscription = Inscription::findOrFail($id);
          $inscription->comment=$request->comment;
          $inscription->save();

	  foreach ($inscription->competitors as $competitor) {
            Mail::to($competitor->email,$competitor->name)  
            ->send(new SendCommentMail($inscription));
          }

          Alert()->success('Se ha enviado un comentario de manera exitosa','! Excelente ¡')->autoclose(6000);

          return redirect()->route('inscription.show',$id);

    }

    public function getCategoryType($birthdate,$gender){
        $age=\Carbon\Carbon::createFromFormat('Y-m-d', $birthdate)->age;
        if($age <=18 && $gender == 'M'){
            return 'Infantil Masculino';
        }elseif($age <=18 && $gender == 'F'){
            return 'Infantil Femenino';
        }elseif($age <= 30 && $gender == 'M'){
            return 'Juvenil Masculino';
        }elseif($age <= 30 && $gender == 'F'){
            return 'Juvenil Femenino';
        }elseif($age >= 31 && $gender == 'M'){
            return 'Adulto Masculino';
        }elseif($age >= 31 && $gender == 'F'){
            return 'Adulto Femenino';
        }
    }
}
