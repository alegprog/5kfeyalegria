<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Report\SearchReport;
use App\Inscription;
use \Carbon\Carbon;
use PDF;

class ReportController extends Controller
{
    public function index(){
      $type_page='index';
      $date_init=Null;
      $date_end=Null;
      $inscriptions=Null;	
      return view('app.report.index',compact('type_page','date_init','date_end','inscriptions'));
    }

    public function search(SearchReport $request){
    	$type_page='report';
    	$date_init=Carbon::createFromFormat('d-m-Y H:i:s', $request->date_init.' 00:00:00');
    	$date_end=Carbon::createFromFormat('d-m-Y H:i:s', $request->date_end.' 23:59:59');
    	//->toDateString();

    	//dd($date_end);

    	//$date_end='31/08/2017 23:59:59';

    	$inscriptions = Inscription::with(['competitors' => function ($query) {  
    		$query->with(['size' => function ($query) {          
            }]);         
        }])->with(['payments' => function ($query) {
            $query->with(['bank' => function ($query) {          
            }]);          
        }])->whereBetween('created_at',[$date_init,$date_end])->get();

        return view('app.report.index',compact('type_page','date_init','date_end','inscriptions'));

        //dd($inscriptions);
    }

    public function pdfticket($id){
      $inscription = Inscription::with(['competitors' => function ($query) {  
        $query->with(['size' => function ($query) {          
            }]);         
        }])->with(['payments' => function ($query) {
            $query->with(['bank' => function ($query) {          
            }]);          
        }])->findOrFail($id);

        $pdf = PDF::loadview('app.report.pdftickets', ['data'=>$inscription]);

        return $pdf->stream('ticket-'.str_pad($inscription->id,7,"0",STR_PAD_LEFT).'.pdf');
    }

    public function frontpdfticket($code){
      $inscription = Inscription::with(['competitors' => function ($query) {  
        $query->with(['size' => function ($query) {          
            }]);         
        }])->with(['payments' => function ($query) {
            $query->with(['bank' => function ($query) {          
            }]);          
        }])->where('code',$code)->first();

        if(!$inscription){
            abort(404);
        }

        $pdf = PDF::loadview('app.report.pdftickets', ['data'=>$inscription]);

        return $pdf->stream('ticket-'.str_pad($inscription->id,7,"0",STR_PAD_LEFT).'.pdf');
    }
}
