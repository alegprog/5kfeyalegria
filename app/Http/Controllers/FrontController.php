<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Contact\StoreContactRequest;
use App\Mail\SendContact as SendContactMail;
use Mail;
use App\Contact;
use App\Page;
use App\Sponsor;
use App\Slider;

class FrontController extends Controller
{
    public function index(){

      $page = Page::where('type','home')->first();
      $sponsors = Sponsor::get();
      $sliders = Slider::get();
    	
    	return view('front.home.index',compact('page','sponsors','sliders'));
    }

    public function help(){
      $sponsors = Sponsor::get();

      $page = Page::where('type','help')->first();
    	
    	return view('front.help.index',compact('page','sponsors'));
    }

    public function info(){
      $sponsors = Sponsor::get();

      $page = Page::where('type','info')->first();
      
      return view('front.info.index',compact('page','sponsors'));
    }

    public function terms(){
      $sponsors = Sponsor::get();

      $page = Page::where('type','terms')->first();
      
      return view('front.terms.index',compact('page','sponsors'));
    }

    public function contact(){
      $sponsors = Sponsor::get();
    	
    	return view('front.contact.index',compact('sponsors'));
    }

    public function contactSend(StoreContactRequest $request){

      $contact = new Contact();
      $contact->name = $request->name;
      $contact->email = $request->email;
      $contact->phone = $request->phone;
      $contact->message = $request->message;
      $contact->save();

      Mail::to('info@5kporlaeducacion.com','5K por la educación')
        ->bcc('alejandrogarcia15@gmail.com')
	->bcc('rborges@uejavier.com')
        ->send(new SendContactMail($contact));

      return redirect()->route('front.contact')->with('success', trans('message.send'));
    }
}
