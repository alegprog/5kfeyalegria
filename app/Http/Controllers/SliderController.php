<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Slider\StoreSlider;
use App\Http\Requests\Slider\UpdateSlider;
use App\Slider;
use Storage;

class SliderController extends Controller
{
    public function index()
	{
	    $sliders=Slider::paginate(10);
	    return view('app.slider.index',compact('sliders'));
	}

	public function create()
	{
	    return view('app.slider.create');
	}

	public function store(StoreSlider $request)
	{
      $slider = new Slider();
      $slider->name=$request->name;
      $slider->banner='';      
      if($slider->save()){
       if ($request->hasFile('banner')){
         $path = $request->file('banner')->store('slider'.'/'.$slider->id,'slider');
         $slider->banner=$path;
         $slider->save();
       }
     }

      return redirect()->route('slider')->with('success', trans('message.store'));

	}

	public function edit($id){
    	$slider=Slider::findOrFail($id);
    	return view('app.slider.edit', compact('slider'));
    }

    public function Update(UpdateSlider $request,$id)
   {
       $slider = Slider::findOrFail($id);
       $slider->name=$request->name;      
       if($slider->save()){
        if ($request->hasFile('banner')){
           //dd('entro');
           $banner =$slider->banner;
           if(Storage::disk('slider')->has($banner)){
             Storage::disk('slider')->delete($banner);
           }
           $path = $request->file('banner')->store('slider'.'/'.$slider->id,'slider');
           $slider->banner=$path;
           $slider->save();
         }
      }

       return redirect()->route('slider')->with('success', trans('message.update'));

   }

   public function remove($id){

        $slider=Slider::withTrashed()->findOrFail($id);   
        $slider->delete();   

        return redirect()->route('slider')->with('success', trans('message.delete'));

    }
}
