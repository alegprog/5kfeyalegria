<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PageController extends Controller
{
    public function edit($name){

    	$page=Page::where('type',$name)->first();

    	if(!$page){
    		return redirect('home');
    	}

    	$title = trans('module.page.'.$name);
    	$icon = trans('module.page.icon-'.$name);



    	return view('app.page.edit',compact('page','title','icon'));
    }

    public function update(Request $request, $id){
    	//dd($request->all());
    	$page = Page::findOrFail($id);
    	$page->content = $request->content;
    	$page->save();

    	return redirect()->back();
    }
}
