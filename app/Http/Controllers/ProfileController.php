<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\Profile\UpdateProfile;
use App\Http\Requests\Profile\UpdatePassword;

class ProfileController extends Controller
{
  public function index(){
    $user= auth()->user();
    return view('app.profile.index',compact('user'));
  }

  public function update(UpdateProfile $request){
    $user_id=auth()->user()->id;
    $user = User::findOrFail($user_id);
    $user->name= $request->name;
    $user->email= $request->email;
    $user->save();

    return redirect()->route('profile')->with('success', trans('message.update'));
  }

  public function updatePassword(UpdatePassword $request){
    $user_id=auth()->user()->id;
    $user = User::findOrFail($user_id);
    $user->password= bcrypt($request->password);
    $user->save();

    return redirect()->route('profile')->with('success', trans('message.update'));
  }
}
