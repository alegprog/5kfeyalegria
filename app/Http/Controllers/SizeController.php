<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Size\StoreSize;
use App\Http\Requests\Size\UpdateSize;
use App\Size;

class SizeController extends Controller
{
  public function index()
  {
      $sizes=Size::paginate(10);
      return view('app.size.index',compact('sizes'));
  }

  public function create()
  {
      return view('app.size.create');
  }

  public function store(StoreSize $request)
  {
      $size = new Size();
      $size->name=$request->name;      
      $size->save();

      return redirect()->route('size')->with('success', trans('message.store'));

  }

  public function edit($id){
    $size=Size::findOrFail($id);
    return view('app.size.edit', compact('size'));
  }

  public function update(UpdateSize $request, $id)
  {
      $size = Size::findOrFail($id);
      $size->name=$request->name;
      $size->save();

      return redirect()->route('size')->with('success', trans('message.update'));

  }

  public function remove($id){

        $size=Size::withTrashed()->findOrFail($id);   
        $size->delete();   

        return redirect()->route('size')->with('success', trans('message.delete'));

    }
}
