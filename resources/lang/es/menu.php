<?php
return [
  'company'=>'Empresa',
  'companies'=>'Empresas',
  'favorite'=>'Favoritos',
  'dashboard'=>'Panel',
  'category'=>'Categoria',
  'login'=>'Iniciar sesión',
  'register'=>'Registrarte',
  'logout'=>'Cerrar Session',
  'profile'=>'Perfil',
  'home'=>'Inicio',
  'search'=>'Buscar',
];
