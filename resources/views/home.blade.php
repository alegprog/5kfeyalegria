@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div style="font-size: 20px;" class="panel-heading"><i class="fa fa-tachometer"></i> <b>Dashboard</b></div>

                <div class="panel-body">

                  <div class="row">

                  @if(auth()->user()->type=='master' || auth()->user()->type=='manager')
                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Paginas</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('page','home') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-home"></i> <br> Inicio</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('page','help') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-question-circle"></i> <br>Ayuda</a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Paginas</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('page','info') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-info-circle"></i> <br> Información</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('page','terms') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-file"></i> <br>Terminos</a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>


                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Bancos</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('bank') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-list-alt"></i> <br> Listado</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('bank.create') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-plus"></i> <br>Agregar</a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Tallas</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('size') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-list-alt"></i> <br> Listado</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('size.create') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-plus"></i> <br>Agregar</a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    

                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Patrocinantes</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('sponsor') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-list-alt"></i> <br> Listado</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('sponsor.create') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-plus"></i> <br>Agregar</a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Slider</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('slider') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-list-alt"></i> <br> Listado</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('slider.create') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-plus"></i> <br>Agregar</a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Usuarios</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('user') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-list-alt"></i> <br> Listado</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('register') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-plus"></i> <br>Agregar</a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Reportes</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('report') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-list-alt"></i> <br> Reporte</a>
                            </div>                            
                          </div>
                        </div>

                      </div>
                    </div>

                    @endif

                    @if(auth()->user()->type=='master' || auth()->user()->type=='manager' || auth()->user()->type=='operator')

                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Inscripción</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('inscription') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-list-alt"></i> <br> Listado</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('inscription.create') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-plus"></i> <br>Inscribir</a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    @endif

                    <div class="col-md-4 col-sm-4">
                      <div class="panel panel-primary" >
                        <div style="font-size: 20px;" class="panel-heading text-center"><b>Perfil</b></div>

                        <div class="panel-body">
                          <div class="row">
                            <div class="col-xs-6">
                              <a href="{{ route('profile') }}" class="btn btn-primary btn-block"><i style="font-size: 30px;" class="fa fa-address-card-o"></i> <br> Ver Perfil</a>
                            </div>
                            <div class="col-xs-6">
                              <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();"
                                   class="btn btn-primary btn-block">
                                   <i style="font-size: 30px;" class="fa fa-sign-out"></i>
                                   <br>Cerrar Sesión
                              </a>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                  <div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
