<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> 
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        @yield('style')
        

        <!-- Styles -->
        <style>

        body{
            font-family: 'Lato', sans-serif;
            font-weight: 500;
            background-color: #f5f5f5;
            background-color: #f1f1f1;
            letter-spacing: .07em;
        }

        .nav-index{
            /*padding-bottom: 20px !important;
            background-color:  #ffb300;*/
            /*box-shadow-bottom: 1px 0 2px 2px rgba(42,42,42,.25);]*/
        }

        .nav-main {
            font-family: 'Lato', sans-serif; 
            font-weight: 700;
            margin-bottom: 0;
            padding-left: 0;
            list-style: none;
            margin-top: 30px;  
            box-shadow-bottom: 1px 0 2px 2px rgba(42,42,42,.25);  
            border-top: 1px solid #E8C26C;   
            border-bottom: 1px solid #E8C26C;    
            letter-spacing: .14em; 
        }

        .nav-main::after, .nav-main::before {
            content: " ";            
        }

        .nav-main > li {            
            padding: 8px;
            font-size: 18px !important;
        }
        .nav-main > li, .nav-main > li > a {
            display: inline-block;
            position: relative;
            
            color: #FFF;            
            color:rgba(0,0,0,.87);
            color: #525252;
        }

        .site-footer {
            background-color: #525252;
            /*text-align: center;*/
            font-family: Lato,sans-serif;
            font-size: 13px;
            letter-spacing: .12em;
            font-weight: 500;
            padding: 60px 0;
            color:#FFF;
        }

        .site-footer h6 {
    color: #fff;
    font-size: 18px;
    margin-top: 0;
    letter-spacing: 2px;
    text-transform: uppercase;
}

.site-footer a{
  color: #fff;
}

.site-footer a:hover{
    color: #f4645f;
}

.footer-links {
    padding-left: 0;
    list-style: none;
}

.site-footer .social-icons {
    text-align: left;
}

.social-icons li {
    display: inline-block;
    margin-bottom: 4px;
}

.social-icons {
    padding-left: 0;
    margin-bottom: 0;
    list-style: none;
}

.site-footer .social-icons a {
    width: 40px;
    height: 40px;
    line-height: 40px;
    margin-left: 6px;
    margin-right: 0;
    border-radius: 100%;
    background-color: #33353d;
}

.social-icons a {
    background-color: #eceeef;
    color: #818a91;
    font-size: 16px;
    display: inline-block;
    line-height: 44px;
    width: 44px;
    height: 44px;
    margin-right: 8px;
    border-radius: 100%;
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
}

.social-icons a.facebbok{
  color: #00aced;
}

.social-icons a.twitter{
  color: #00aced;
}

.social-icons a.ico{
  color: #f4645f;
}

.social-icons a.ico:hover {
    background-color: #FFF;
}

.login-block, .post header h2, .post header time, .read-more, .social-icons a, .team-member, .widget-title {
    text-align: center;
}

.card-header{
    font-weight: 600;
}





            
        </style>
    </head>
    <body>
      <header class="text-center" style="background-color: #FFFFFF;">
        <div class="line-top" style="background-color: #E71E37; height: 6px;"></div>
        <div class="logo"><img src="{{ asset('assets/image/5k_logo.jpg') }}" width="150"></div>
        <div class="title"><h1 style="font-family: Lato;margin: 1px;padding: 1px; font-size: 38px;font-weight: 900; font-style:italic; color:#E71E37;">5K por la educación</h1></div>
        
        <div class="nav-index">
            <ul class="nav-main">
              <li role="presentation" class="active"><a href="#">Inscripción</a></li>
              <li role="presentation"><a href="#">Ayuda</a></li>
              <li role="presentation"><a href="#">Contacto</a></li>
            </ul>
        <div>
      </header>
      <div class="container">

      <br>
      <div class="col-md-12">
       <div class="row">

        <div class="card">
            <div class="card-header">
                Inscricipción
            </div>
            <div class="card-block">
                Esta es una prueba de contenido
            </div>
        </div>
           
       </div>
      </div>

      </div>
      <footer class="site-footer">
        <div class="container">
        <div class="row" id="about">
            <div class="col-sm-6 col-md-4">
                <h6>Contacto</h6>
                <p class="text-justify f-family"><i class="fa fa-phone"></i> 0426-2360894</p>
                <p class="text-justify"><i class="fa fa-envelope"></i> info@5kporlaeducacion.com</p>
            </div>

            <div class="visible-xs">
              <div class="clearfix"></div>
              <hr>
            </div>

            <div class="col-sm-6 col-md-4">
                <h6>Menú</h6>
                <ul class="footer-links">
                    <li><a href="{{ url('/') }}" >Inicio</a></li>
                    <li><a href="{{ url('/contact') }}" >Contacto</a></li>
                    @if (Auth::guest())

                    @else

                    @endif
                </ul>
            </div>

            {{--<div class="visible-xs">
              <div class="clearfix"></div>
              <hr>
            </div>

            <div class="visible-sm">
              <div class="clearfix"></div>
              <hr>
            </div>--}}

            {{--<div class="col-sm-6 col-md-3">
                <h6>Nosotros</h6>
                <ul class="footer-links">
                    <li><a href="{{ url('/nosotros') }}" target="_blank">¿Quiénes somos?</a></li>
                    <li><a href="{{ url('/como-funciona') }}" target="_blank">¿Cómo funciona?</a></li>
                    <li><a href="{{ url('/terminos-condiciones') }}" target="_blank">Términos y condiciones</a></li>
                </ul>
            </div>--}}

            <div class="visible-sm visible-xs">
              <div class="clearfix"></div>
              <hr>
            </div>

            <div class="col-sm-6 col-md-4">
                <h6>Siguenos</h6>
                <p>

                  <ul class="social-icons">
                      <li><a class="ico" href="https://www.facebook.com/OpenClosed24-115253269073726" target="_blank"><i class="fa fa-facebook"></i></a></li>
                      <li><a class="ico" href="https://plus.google.com/u/0/104035886628433983658" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                      <li><a class="ico" href="https://www.instagram.com/openclosed24/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                      <li><a class="ico" href="https://www.youtube.com/channel/UCwSJQ3cxyi8YVC-mGcE1wNw" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  </ul>

                </p>
            </div>

            <div class="">
              <div class="clearfix"></div>
              <hr>
            </div>
        </div>

        
    </div>
    <!-- END Top section -->



    <!-- Bottom section -->
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Derechos reservados &copy; 2017 por <a href="{{url('/')}}" target="_blank">5kporlaeducacion</a>.</p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
              <p class="">Desarrollado por: <a title="Programador y desarrollador web" target="_blank" href="http://aleprog.com.ve">Alejandro Garcia</a></p>
            </div>
        </div>
    </div>
    <!-- END Bottom section -->
      </footer>
    </body>
</html>