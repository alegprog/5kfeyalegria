<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title','5k por la educación de calidad a beneficio de Fe y Alegría')</title>
	<meta name="description" content="5k POR LA EDUACIÓN, la carrera es un evento de carácter benéfico - familiar, a favor de FE Y ALEGRIA, Corremos 5K y más por ayudar a nuestros niños y niñas de Fe y Alegría, por el derecho que tienen todas las personas a la educación y una educación de calidad.">
	<meta name="keywords" content="run5k, 5k, fe y alegría, fe y alegría ecuador, corremos 5k, carrera 5k, ecuador, educación ecuador, silla roja, sillaroja5k.com, silla roja 5k, niños y niñas de Fe y Alegría, educación de calidad, 5k por la educación, ayudar a nuestros niños y niñas, ayudar a niños y niñas, educación niños y niñas, educación ecuador niños y niñas, niños y niñas ecuatorianas, ecuatorianos, Benéfico-Familia, 5k POR LA EDUACIÓN, evento familia, evento a beneficio, evento a beneficio de fe y alegría, guayaquil, uejavier, SillaRoja, Carrera ecuador, carrera guayaquil, Carrera5k, educación guayaquil, guayaquil, jesuitas ecuador, Movimiento de Educación Popular y de Promoción Social, formación de hombres y mujeres nuevos, feyalegria.org.ec,  Federación Fe y Alegría, FLACSI, IRFEYAL, quito"> 
	<meta name="author" content="Alejandro Garcia - Programador y Desarrollador Web (Frontend-Backend) - alejandrogarcia15@gmail.com - www.aleprog.com.ve">

	<meta property="og:title" content="@yield('title','5k por la educación de calidad a beneficio de Fe y Alegría')">
	<meta property="og:description" content="5k POR LA EDUACIÓN, la carrera es un evento de carácter benéfico - familiar, a favor de FE Y ALEGRIA, Corremos 5K y más por ayudar a nuestros niños y niñas de Fe y Alegría, por el derecho que tienen todas las personas a la educación y una educación de calidad.">
	<meta property="og:image" content="{{ asset('assets/image/5k_logo.jpg') }}">

	<meta name="twitter:title" content="@yield('title','5k por la educación de calidad a beneficio de Fe y Alegría')">
	<meta name="twitter:description" content="5k POR LA EDUACIÓN, la carrera es un evento de carácter benéfico - familiar, a favor de FE Y ALEGRIA, Corremos 5K y más por ayudar a nuestros niños y niñas de Fe y Alegría, por el derecho que tienen todas las personas a la educación y una educación de calidad.">
	<meta name="twitter:image" content="{{ asset('assets/image/5k_logo.jpg') }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> 
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/front.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        @yield('style')
	</head>

    <body>

	    @include('pages.front_header')

	    @yield('content')

	    @include('pages.front_footer')

        @yield('modal')

	    <script src="{{ asset('js/app.js') }}"></script>

        <script type="text/javascript">
            var csrfToken = $('[name="csrf-token"]').attr('content');
            var token = $('[name="_token"]').attr('value');

            function refreshToken(){
                $.get('/refresh-csrf').done(function(data){
                    $('[name="csrf_token"]').attr('content',data); // the new token
                    $('[name="_token"]').attr('value',data);
                    //console.log(csrfToken,token);
                });
            }

            setInterval(refreshToken, 3600000); // 1 hour 3600000

        </script>

        <script type="text/javascript">
        $(window).load(function(){
                CargarImagenes();
        });
            
        function CargarImagenes()
        {
            $(".post-carga").each(function(){
                $(this).attr('src', $(this).data('src')).load(function(){
                    $(this).fadeIn();
                });
            })  
        }
    </script>

	    @yield('script')

     </body>
</html>
