<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @section('meta')
    <meta name="description" content="Christianroehrdanz Conoce la gran variedad en camisas, la mejor marcas para vestir bien">
    @show
    <meta property="og:title" content="Christianroehrdanz Conoce la gran variedad en camisas, la mejor marcas para vestir bien">
    <meta property="og:description" content="Christianroehrdanz Conoce la gran variedad en camisas, la mejor marcas para vestir bien">
    <meta property="og:image" content="{{asset('images/bg-banner1.jpg')}}">
    <meta property="og:url" content="{{url('/')}}">
    <meta name="twitter:title" content="Christianroehrdanz Conoce la gran variedad en camisas, la mejor marcas para vestir bien">
    <meta name="twitter:description" content="Christianroehrdanz Conoce la gran variedad en camisas, la mejor marcas para vestir bien">
    <meta name="twitter:image" content="{{asset('images/bg-banner1.jpg')}}">
    <meta name="twitter:card" content="summary_large_image">
    <link rel="icon" type="image/png" href="{{asset('images/logos/favicon.png')}}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> {{ config('app.name', 'Christianroehrdanz') }} - @yield('title')</title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
    <link href="{{asset('/css/style.css')}}" rel="stylesheet">

    @yield('css')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        @include('pages.menu')

        @yield('content')

        @include('pages.footer')
    </div>
     

    <!-- Scripts -->
    <script src="{{asset('/js/app.js')}}"></script>
    @yield('script')
</body>
</html>
