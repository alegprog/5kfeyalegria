<div  class="p-t-40 p-b-30 m-t-50 bg-white">

	<header class="section-header"><span class="f-14">nuestros aliados</span> <h2 class="f-20"><i class="fa fa-angle-double-right f-20"></i> Patrocinantes</h2></header>

  <div class="clearfix"></div>
  <div class="container">
    <div class="row">
    @php
      $count=1;
    @endphp
    @foreach($sponsors as $sponsor)

    <div class="col-sm-6 col-md-4 col-lg-3" >
      <img src="{{asset('assets/file/'.$sponsor->logo)}}" alt="Patrocinante {{$sponsor->name}}" title="{{$sponsor->name}}" class="img-responsive sponsor_official m-b-30 img-sponsor">
    </div>
      @php
        if($count%2==0){
          echo '<div class="visible-sm clearfix"></div>';
        }
        
        if($count%3==0){
          echo '<div class="visible-md clearfix"></div>';
        }

        if($count%4==0){
          echo '<div class="visible-lg clearfix"></div>';
        }
        $count=$count+1;
      @endphp
    @endforeach
   </div>
  </div>

</div>