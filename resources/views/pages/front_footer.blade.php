<div style="background-color: #FFFFFF;" class="p-t-25">
 <div class="container">
   <div class="row">    
    <div class="col-sm-6 col-md-4">
      <img src="{{asset('assets/image/logo_javier.png')}}" style="max-width: 290px;" alt="Logo Javier" class="img-responsive sponsor_official m-b-30 post-carga">
    </div>
    <div class="col-sm-6 col-md-4">
      <img src="{{asset('assets/image/logo_silla_roja.png')}}" style="max-width: 200px;" alt="Logo Silla Roja" class="img-responsive sponsor_official m-b-30 post-carga">
    </div>
    <div class="visible-sm clearfix"></div>
    <div class="col-sm-6 col-md-4">
      <img src="{{asset('assets/image/logo_fasa.png')}}" style="max-width: 290px;" alt="Logo FASA" class="img-responsive sponsor_official m-b-30 post-carga">
    </div>
   </div>
 </div>
</div>

<footer class="site-footer">
    <div class="container">
    <div class="row" id="about">
        <div class="col-sm-6 col-md-4">
            <h6>Contacto</h6>
            <p class="text-justify f-family"><i class="fa fa-phone"></i> 0958993412 – 0960605500</p>
            <p class="text-justify"><i class="fa fa-envelope"></i> info@5kporlaeducacion.com</p>
        </div>

        <div class="visible-xs">
          <div class="clearfix"></div>
          <hr>
        </div>

        <div class="col-sm-6 col-md-4">
            <h6>Menú</h6>
            <ul class="footer-links">
                <li><a href="{{ url('/') }}" >Inicio</a></li>
                <li><a href="{{ url('/inscription') }}" >Inscripción</a></li>
                <li><a href="{{ url('/help') }}" >Ayuda</a></li>
                <li><a href="{{ url('/contact') }}" >Contacto</a></li>
                @if (Auth::guest())

                @else

                @endif
            </ul>
        </div>

        {{--<div class="visible-xs">
          <div class="clearfix"></div>
          <hr>
        </div>

        <div class="visible-sm">
          <div class="clearfix"></div>
          <hr>
        </div>--}}

        {{--<div class="col-sm-6 col-md-3">
            <h6>Nosotros</h6>
            <ul class="footer-links">
                <li><a href="{{ url('/nosotros') }}" target="_blank">¿Quiénes somos?</a></li>
                <li><a href="{{ url('/como-funciona') }}" target="_blank">¿Cómo funciona?</a></li>
                <li><a href="{{ url('/terminos-condiciones') }}" target="_blank">Términos y condiciones</a></li>
            </ul>
        </div>--}}

        <div class="visible-sm visible-xs">
          <div class="clearfix"></div>
          <hr>
        </div>

        <div class="col-sm-6 col-md-4">
            <h6>Siguenos</h6>
            <p>

              <ul class="social-icons">
                  <li><a class="ico" href="https://www.facebook.com/pastoraljavier/" target="_blank" title="Pastoral Javier Facebook" ><i class="fa fa-facebook"></i></a></li>
                  {{--<li><a class="ico" href="https://plus.google.com/u/0/104035886628433983658" target="_blank"><i class="fa fa-google-plus"></i></a></li>--}}
                  <li><a class="ico" href="https://www.instagram.com/pastoraljavier/" target="_blank" title="Pastoral Javier Instagram" ><i class="fa fa-instagram"></i></a></li>
                  {{--<li><a class="ico" href="https://www.youtube.com/channel/UCwSJQ3cxyi8YVC-mGcE1wNw" target="_blank"><i class="fa fa-twitter"></i></a></li>--}}
              </ul>

            </p>
            
        </div>

        <div class="">
          <div class="clearfix"></div>
          <hr>
        </div>
    </div>

    
</div>
<!-- END Top section -->



<!-- Bottom section -->
<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Derechos reservados &copy; 2017 por <a href="{{url('/')}}" target="_blank">5kporlaeducacion</a>.</p>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <p class="">Desarrollado por: <a title="Programador y desarrollador web" target="_blank" href="http://aleprog.com.ve">Alejandro Garcia</a></p>
        </div>
    </div>
</div>
<!-- END Bottom section -->
</footer>