
<footer class="site-footer">
    <!-- Top section -->
    <div class="container">
        <div class="row" id="about">
            <div class="col-sm-6 col-md-4">
                <h6>Contacto</h6>
                <p class="text-justify f-family"><i class="fa fa-whatsapp"></i> (0426)2360894</p>
                <p class="text-justify"><i class="fa fa-envelope"></i> info@christianroehrdanz.com</p>
            </div>

            <div class="visible-xs">
              <div class="clearfix"></div>
              <hr>
            </div>

            <div class="col-sm-6 col-md-4">
                <h6>Menú</h6>
                <ul class="footer-links">
                    <li><a href="{{ url('/') }}" >Inicio</a></li>
                    <li><a href="{{ url('/contact') }}" >Contacto</a></li>
                    @if (Auth::guest())

                    @else

                    @endif
                </ul>
            </div>

            <div class="visible-xs">
              <div class="clearfix"></div>
              <hr>
            </div>

            <div class="visible-sm">
              <div class="clearfix"></div>
              <hr>
            </div>

            {{--<div class="col-sm-6 col-md-3">
                <h6>Nosotros</h6>
                <ul class="footer-links">
                    <li><a href="{{ url('/nosotros') }}" target="_blank">¿Quiénes somos?</a></li>
                    <li><a href="{{ url('/como-funciona') }}" target="_blank">¿Cómo funciona?</a></li>
                    <li><a href="{{ url('/terminos-condiciones') }}" target="_blank">Términos y condiciones</a></li>
                </ul>
            </div>--}}

            <div class="visible-xs">
              <div class="clearfix"></div>
              <hr>
            </div>

            <div class="col-sm-6 col-md-4">
                <h6>Siguenos</h6>
                <p>

                  <ul class="social-icons">
                      <li><a class="twitter" href="https://www.facebook.com/OpenClosed24-115253269073726" target="_blank"><i class="fa fa-facebook"></i></a></li>
                      <li><a class="twitter" href="https://plus.google.com/u/0/104035886628433983658" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                      <li><a class="twitter" href="https://www.instagram.com/openclosed24/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                      <li><a class="twitter" href="https://www.youtube.com/channel/UCwSJQ3cxyi8YVC-mGcE1wNw" target="_blank"><i class="fa fa-twitter"></i></a></li>
                  </ul>

                </p>
            </div>
        </div>

        <hr>
    </div>
    <!-- END Top section -->

    <!-- Bottom section -->
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Derechos reservados &copy; 2017 por <a href="{{url('/')}}" target="_blank">5kporlaeducacion</a>.</p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
              <p class="">Desarrollado por: <a title="Programador y desarrollador web" target="_blank" href="http://aleprog.com.ve">Alejandro Garcia</a></p>
            </div>
        </div>
    </div>
    <!-- END Bottom section -->

</footer>
