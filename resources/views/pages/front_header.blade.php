<header class="text-center" >
    <div class="line-top"></div>
    <div class="logo"><img src="{{ asset('assets/image/5k_logo.jpg') }}" width="130"></div>
    <div class="title"><h1>5K por la educación</h1></div>
    
    <div class="nav-index">
        <ul class="nav-main">
          <li role="presentation"><a href="{{ url('/') }}">Inicio</a></li>
          <li role="presentation"><a href="{{ url('/inscription') }}">Inscripción</a></li>
          <li role="presentation"><a href="{{ url('/info') }}">Carrera</a></li>
          <li role="presentation"><a href="{{ url('/help') }}">Ayuda</a></li>
          <li role="presentation"><a href="{{ url('/contact') }}" >Contacto</a></li>
        </ul>
    <div>
</header>