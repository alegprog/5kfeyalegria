@extends('layouts.app')

@section('style')
  {!! Html::style('https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css') !!} 
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
            <li class="active">Inscripciones</li>
          </ol>
        </div>
        <div class="col-md-12 p-b-10">
          {!! link_to_route('inscription.create', trans('action.enroll'), $parameters = [], ['class'=>'btn btn-primary btn-l pull-right']) !!}
          <br><br>
        </div>
        <div class="col-md-12">
          @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><i class="fa fa-check"></i> {{trans('message.success')}}</strong> {{ session('success') }}
            </div>
          @endif
            <div class="panel panel-default">

                <div class="panel-heading"><i class="fa fa-list-alt"></i> {{trans('module.inscriptions')}}</div>

                <div class="panel-body">
                  <table id="datatable-inscription" class="table table-striped">
                    <thead>
                     <tr>                      
                      <th>
                        {{ trans('label.code') }}
                      </th>
                      <th>
                        {{ trans('label.date') }}
                      </th>   
                      <th>
                        {{ trans('label.type') }}
                      </th>
                      <th>
                        {{ trans('label.identification') }}
                      </th>
                      <th>
                        {{ trans('label.competitor') }}
                      </th>
                      <th>
                        {{ trans('label.status') }}
                      </th>
                      <th>
                        {{ trans('label.registered_by') }}
                      </th>                                          
                      <th style="width:70px" >
                        {{ trans('label.action') }}
                      </th>
                     </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                  
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')  
  {!! Html::script('https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js') !!}
  {!! Html::script('https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js') !!}

  <script>

    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });
    $('#datatable-inscription').DataTable({
           //"sDom": "<'row mb-1'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6 center'p>>",
           renderer: 'bootstrap',
            serverSide: true,
            processing: true,
            ajax: '/dashboard/inscription/data',
            columns: [                
                {data: 'zero_code'},
                {data: 'created_at_es'},
                {data: 'type_es'},
                {data: 'competitors[0].identification'},
                {data: 'competitors[0].name'},
                {data: 'status_es'},
                {data: 'registered_by'},
                {data: 'action',orderable: false, searchable: false},
            ],
            order: [[ 1, 'desc' ]],
            //oSearch: {"sSearch": "Pendiente"},
            lengthMenu:[10,15,20,50,100],
            pageLength:20,
            language: {
               processing:     "Procesando ...",
               search:         '<span class="glyphicon glyphicon-search"></span>',
               searchPlaceholder: "BUSCAR",
               lengthMenu:     "Mostrar _MENU_ Registros",
               info:           "Mostrando _START_ a _END_ de _TOTAL_ Registros",
               infoEmpty:      "Mostrando 0 a 0 de 0 Registros",
               infoFiltered:   "(filtrada de _MAX_ registros en total)",
               infoPostFix:    "",
               loadingRecords: "...",
               zeroRecords:    "No se encontraron registros coincidentes",
               emptyTable:     "No hay datos disponibles en la tabla",
               paginate: {
                   first:      "Primero",
                   previous:   "Anterior",
                   next:       "Siguiente",
                   last:       "Ultimo"
               },
               aria: {
                   sortAscending:  ": habilitado para ordenar la columna en orden ascendente",
                   sortDescending: ": habilitado para ordenar la columna en orden descendente"
               }
            }
        });

  </script>
@endsection