@extends('layouts.app')

@section('style')
	{!! Html::style('assets/vendors/sweetalert/sweetalert.min.css') !!} 
	<style type="text/css">
		
@media 
	only screen and (max-width: 768px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {

		table, thead, tbody, th, td, tr { 
			display: block; 
		}
	
		table {width:100%;}
		thead {display: none;}		
		td{display: block;  text-align:center;}
		td:before { 
		    content: attr(data-th); 
		    display: block;
		    text-align:center;  
		  }

		table {width:100%;}
		thead {display: none;}		
		/*td{display: inline-flex;  text-align:center; width: 100%;}*/
		td{display: inline-flex;  text-align:center; width: 100%;}
		td:before { 
		    content: attr(data-th)": ";
		    padding-right: 10px; 
		    display: block;
		    text-align:center;  
		    font-weight: bold;
		    width: 38%
		  }

	}

	@media 
	only screen and (max-width: 600px),
	(min-device-width: 600px) and (max-device-width: 600px)  {
		td{display: block;  text-align:center; width: 100%;}

		td:before{
			width: 100%;
		}
	}

	@media 
	only screen and (max-width: 320px),
	(min-device-width: 320px) and (max-device-width: 320px)  {

	}

	</style>
@endsection

@section('content')

<div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
          <li><a href="{{route('inscription')}}">Inscripción</a></li>
          <li class="active">Detalle</li>
        </ol>
      </div>

        <div class="col-md-12">
        @if (session('danger'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><i class="fa fa-check"></i> {{trans('message.danger')}}</strong> {{ session('danger') }}
            </div>
          @endif
            <div class="panel panel-default">
                <div class="panel-heading f-18"><i class="fa fa-file-text" aria-hidden="true"></i> &nbsp;
 Inscripción: <span class="text-info">{{str_pad($inscription->id,7,"0",STR_PAD_LEFT)}}</span></div>

                <div class="panel-body" style="padding-top: 20px;">

                <div class="row">

                    <div class="col-xs-12 col-sm-3 col-md-2">
                        @if($inscription->status=='approved')
                    		<a class="btn btn-default" href="{{route('inscription.ticket',$inscription->id)}}" target="_blank"><i class="fa fa-print f-20"></i> Comprobante</a>
                    	@endif
                    </div>  
                    <div class="clearfix visible-xs" style="margin-top:40px;"></div>                
	                <div class="col-xs-12 col-sm-6 col-md-8 text-right">
	                   <i class="fa fa-calendar f-20"></i> &nbsp;{{$inscription->created_at_es}}
	                </div>
	                <div class="clearfix visible-xs" style="margin-top:40px;"></div>
	                <div class="col-xs-12 col-sm-3 col-md-2 text-right">
	                   @if($inscription->status=='pending')	
	                     <span class="label label-warning" style="padding: 8px 12px; font-size: 16px;">
	                     	<i class="fa fa-exclamation-circle"></i> {{trans('label.'.$inscription->status)}}
	                     </span> 
	                   @elseif($inscription->status=='approved')
	                   	 <span class="label label-success" style="padding: 8px 12px; font-size: 16px;" >
	                        <i class="fa fa-check"></i>	{{trans('label.'.$inscription->status)}}
	                     </span> 
	                   @elseif($inscription->status=='rejected')
	                   	 <span class="label label-danger" style="padding: 8px 12px; font-size: 16px;">
	                     	<i class="fa fa-ban"></i> {{trans('label.'.$inscription->status)}}
	                     </span> 
	                   @elseif($inscription->status=='process')
	                   	 <span class="label label-primary" style="padding: 8px 12px; font-size: 16px;">
	                     	<i class="fa fa-spinner"></i> {{trans('label.'.$inscription->status)}}
	                     </span>
	                   @endif	                   
	                </div>

	                <div class="col-xs-12 col-sm-12 col-md-12">
	                	<hr>
	                </div>

	                <div class="col-md-12">
	                	<h5 class="f-16 p-l-10"><i class="fa fa-users f-20 text-info"></i> &nbsp;Participantes {{--&nbsp;&nbsp;<i class="fa fa-plus-square f-20 text-info"></i> &nbsp;Cantidad: <span class="">{{count($inscription->competitors)}}</span>--}}</h5>
	                	<table class="table rwd-table table-striped">
	                	  <thead>
	                	   <tr>
	                		<th width="130" class="text-center">{{trans('label.identification')}}</th>
	                		<th class="text-center">{{trans('label.name')}}</th>
	                		<th class="text-center">{{trans('label.email')}}</th>
	                		<th width="60" class="text-center">{{trans('label.size')}}</th>
	                		<th width="70" class="text-center">{{trans('label.gender')}}</th>
	                		<th width="150" class="text-center">{{trans('label.birthdate')}}</th>
	                		<th width="60" class="text-center">{{trans('label.age')}}</th>
	                	   <tr>
	                	  </thead>
	                	  <tbody>
	                	   @foreach($inscription->competitors as $competitor)
	                	   	<tr>
	                	   	 <td data-th="{{trans('label.identification')}}" class="text-center">{{$competitor->identification}}</td>
	                	   	 <td data-th="{{trans('label.name')}}" class="text-center">{{$competitor->name}}</td>
	                	   	 <td data-th="{{trans('label.email')}}" class="text-center">{{$competitor->email}}</td>
	                	   	 <td data-th="{{trans('label.size')}}" class="text-center">{{$competitor->size->name}}</td>
	                	   	 <td data-th="{{trans('label.gender')}}" class="text-center">{{$competitor->gender}}</td>
	                	   	 <td data-th="{{trans('label.birthdate')}}" class="text-center">{{$competitor->birthdate_es}}</td>
	                	   	 <td data-th="{{trans('label.age')}}" class="text-center">{{$competitor->age}}</td>
	                	   	</tr>
	                	   @endforeach
	                	   
	                	  </tbody>
	                	</table>
	                </div>

	                <div class="col-md-12">
	                	<hr>
	                </div>

	                <div class="col-md-12">
	                	<h5 class="f-16 p-l-10"><i class="fa fa-dollar f-20 text-warning"></i> &nbsp;Pagos {{--&nbsp;&nbsp;<i class="fa fa-money f-20 text-warning"></i> &nbsp;Total: <span class="">{{number_format(collect($inscription->payments)->sum('amount'),'2',',','')}}</span>--}}</h5>
	                	<table class="table rw table-striped ">
	                	  <thead>
	                	   <tr>
	                		<th class="text-center">{{trans('label.payment_method')}}</th>
	                		{{--<th class="text-center">{{trans('label.amount')}}</th>--}}
	                		<th class="text-center">{{trans('label.number')}}</th>
	                		<th class="text-center">{{trans('label.bank')}}</th>	                		
	                	   </tr>
	                	  </thead>
	                	  <tbody>
	                	   @foreach($inscription->payments as $payment)
	                	   <tr>
	                		<td data-th="{{trans('label.payment_method')}}" class="text-center">{{$payment->method}}</td>
	                		{{--<td class="text-center">{{number_format($payment->amount,'2',',','')}}</td>--}}
	                		<td data-th="{{trans('label.number')}}" class="text-center">{{$payment->number ? $payment->number : ''}}</td>
	                		<td data-th="{{trans('label.bank')}}" class="text-center">{{$payment->bank ? $payment->bank->name : ''}}</td>	                		
	                	   </tr>
	                	   @endforeach
	                	  </tbody>
	                	</table>

	                </div>

	                <div class="col-md-12">
	                	<hr>
	                </div>

	                @if(auth()->user()->type=='operator' || auth()->user()->type=='master' || auth()->user()->type=='manager')

	                <div class="col-md-12">
	                	<h5 class="f-16 p-l-10"><i class="fa fa-refresh f-20 text-success"></i> &nbsp;Estatus de inscripcón</h5>

	                	<a style="margin-bottom: 20px;" class="btn btn-danger btn-l" href="{{route('inscription.rejected',$inscription)}}" 

	                	    data-ins="ins_{{$inscription->id}}" 

                            data-toggle="confirmation"

                            data-btn-ok-label="Aceptar" data-btn-ok-icon="fa fa-check-square-o"

                            data-btn-ok-class="btn btn-sm btn-danger m-r-10"

                            data-btn-cancel-label="Cancelar"

                            data-btn-cancel-icon="fa fa-chevron-circle-left"

                            data-btn-cancel-class="btn btn-sm btn-default"

                            data-title="¿Está seguro que quiere cambiar el estatus a RECHAZADO?"

                            data-placement="top" data-singleton="true" >

							<i class="fa fa-ban"></i> Rechazar</a> &nbsp;

	                	<a style="margin-bottom: 20px;" class="btn btn-primary btn-l" 

	                	    href="{{route('inscription.process',$inscription)}}"

							data-ins="ins_{{$inscription->id}}"

                            data-toggle="confirmation"

                            data-btn-ok-label="Aceptar" data-btn-ok-icon="fa fa-check-square-o"

                            data-btn-ok-class="btn btn-sm btn-primary m-r-10"

                            data-btn-cancel-label="Cancelar"

                            data-btn-cancel-icon="fa fa-chevron-circle-left"

                            data-btn-cancel-class="btn btn-sm btn-default"

                            data-title="¿Está seguro que quiere cambiar el estatus a EN PROCESO?"

                            data-placement="top" data-singleton="true"> 

                            <i class="fa fa-spinner"></i> En proceso</a> &nbsp;

	                	<a style="margin-bottom: 20px;" class="btn btn-success btn-l" 

	                		href="{{route('inscription.approved',$inscription)}}"

	                		data-ins="ins_{{$inscription->id}}"

                            data-toggle="confirmation"

                            data-btn-ok-href="javascript:void(0)"

                            data-btn-ok-label="Aceptar" data-btn-ok-icon="fa fa-check-square-o"

                            data-btn-ok-class="btn btn-sm btn-success m-r-10"

                            data-btn-cancel-label="Cancelar"

                            data-btn-cancel-icon="fa fa-chevron-circle-left"

                            data-btn-cancel-class="btn btn-sm btn-default"

                            data-title="¿Está seguro que quiere cambiar el estatus a APROBADO?"

                            data-placement="top" data-singleton="true">

                            <i class="fa fa-check"></i> Aprobar</a> &nbsp;

                            <a style="margin-bottom: 20px;" class="btn btn-default btn-l" href="{{route('inscription.remove',$inscription)}}" 

	                	    data-ins="ins_{{$inscription->id}}" 

                            data-toggle="confirmation"

                            data-btn-ok-label="Aceptar" data-btn-ok-icon="fa fa-check-square-o"

                            data-btn-ok-class="btn btn-sm btn-danger m-r-10"

                            data-btn-cancel-label="Cancelar"

                            data-btn-cancel-icon="fa fa-chevron-circle-left"

                            data-btn-cancel-class="btn btn-sm btn-default"

                            data-title="¿Está seguro que quiere ELIMINAR la inscripción?"

                            data-placement="top" data-singleton="true" >

							<i class="fa fa-trash"></i> Eliminar</a> 

	                </div>

	                <div class="col-md-6">
	                
	                {!! Form::model($inscription, ['route' => ['inscription.comment', $inscription->id],'class'=>'form-horizontal']) !!}
                      {{ csrf_field() }}
                      {{ method_field('PUT') }}

                      <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                          {!! Form::label('comment', trans('label.comment'), ['class' => 'col-md-12 ']) !!}

                          <div class="col-md-12 col-sm-12">
                            {!! Form::textarea('comment', null,['class' => 'form-control', 'autofocus'=>'autofocus','cols'=>100,'rows'=>'4', 'maxlength'=>255]) !!}

                              @if ($errors->has('comment'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('comment') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>                     


                      <div class="form-group">
                          <div class="col-md-6 co-sm-6">
                              {!! Form::hidden('id', null) !!}
			       <button class="btn btn-success btn-l" type="submit"><span class="fa fa-book"></span> &nbsp;Guardar</button>
                              {{--{!! Form::button(trans('action.save'),['class'=>'btn btn-success btn-l','type'=>'submit']) !!}--}}
                          </div>
                      </div>
                    {!! Form::close() !!}
                    </div>
		    <div class="col-md-6">
	                
	                {!! Form::model($inscription, ['route' => ['inscription.sendcomment', $inscription->id],'class'=>'form-horizontal']) !!}
                      {{ csrf_field() }}
                      {{ method_field('PUT') }}

                      <div class="form-group{{ $errors->has('comment') ? ' has-error' : '' }}">
                          {!! Form::label('comment', 'Enviar', ['class' => 'col-md-12 ']) !!}

                          <div class="col-md-12 col-sm-12">
                            {!! Form::textarea('comment', null,['class' => 'form-control', 'autofocus'=>'autofocus','cols'=>100,'rows'=>'4', 'maxlength'=>255]) !!}

                              @if ($errors->has('comment'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('comment') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>                     


                      <div class="form-group">
                          <div class="col-md-6 co-sm-6">
                              {!! Form::hidden('id', null) !!}
                              <button class="btn btn-success btn-l" type="submit"><span class="fa fa-envelope"></span> &nbsp;Enviar</button>
                              {{--{!! Form::button('Enviar Comentario',['class'=>'btn btn-success btn-l','type'=>'submit']) !!}--}}
                          </div>
                      </div>
                    {!! Form::close() !!}
                    </div>

                    <div class="col-md-12">
                    <hr>
                    <h5 class="f-16 p-l-10"><i class="fa fa-gift f-20 text-success"></i> &nbsp;Retiro de kit: 
                    	@if($inscription->kits)
                    		<span class="label label-success f-14">SI</span>
                    	@elseif(!$inscription->kits)
                    		<span class="label label-danger f-14">NO</span>
                    	@else
                    		<span class="label label-danger f-14">NO</span>
                    	@endif
                    </h5>

                    	@if($inscription->kits)
	                		<a style="margin-bottom: 20px;" class="btn btn-danger btn-l" href="{{route('inscription.kitsNO',$inscription)}}" 

	                	    data-ins="ins_{{$inscription->id}}" 

                            data-toggle="confirmation"

                            data-btn-ok-label="Aceptar" data-btn-ok-icon="fa fa-check-square-o"

                            data-btn-ok-class="btn btn-sm btn-danger m-r-10"

                            data-btn-cancel-label="Cancelar"

                            data-btn-cancel-icon="fa fa-chevron-circle-left"

                            data-btn-cancel-class="btn btn-sm btn-default"

                            data-title="¿Está seguro que desea ANULAR RETIRO DE KITS?"

                            data-placement="top" data-singleton="true" >

							<i class="fa fa-minus-circle"></i> Anular</a>
						@elseif(!$inscription->kits)
						    <a style="margin-bottom: 20px;" class="btn btn-success btn-l" href="{{route('inscription.kitsOK',$inscription)}}" 

	                	    data-ins="ins_{{$inscription->id}}" 

                            data-toggle="confirmation"

                            data-btn-ok-label="Aceptar" data-btn-ok-icon="fa fa-check-square-o"

                            data-btn-ok-class="btn btn-sm btn-success m-r-10"

                            data-btn-cancel-label="Cancelar"

                            data-btn-cancel-icon="fa fa-chevron-circle-left"

                            data-btn-cancel-class="btn btn-sm btn-default"

                            data-title="¿Está seguro que desea RETIRAR EL KITS?"

                            data-placement="top" data-singleton="true" >

							<i class="fa fa-shopping-bag f-20"></i>&nbsp; Retirar</a>
							
						@endif
                    </div>

	                @endif
                  
                </div>
            </div>
           </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
  <div class="modal fade" id="progress" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title f-20" id="myModalLabel">Procesando solicitud...</h4>
          </div>
          <div class="modal-body">
            <div class="progress">
              <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('script')
  {!! Html::script('assets/vendors/sweetalert/sweetalert.min.js') !!}   
  {!! Html::script('assets/vendors/bootstrap-confirmation/bootstrap-confirmation.min.js') !!} 

  @include('sweet::alert')

  <script>
   

   @if(auth()->user()->type=='master' || auth()->user()->type=='manager')
    $(document).ready(function () {

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.trigger('confirm');
                //alert('enviando');

            }

        });

        $(document).on('confirm', function (e) {

            var ele = e.target;

            //e.preventDefault();

            //return ;
            $('#progress').modal('show');

            window.location=ele.href;


        });
      });
    @endif

    @if($errors->any())
        
          swal({
              title: "Error",
              text: "Los datos introducidos son incorrecto por verifique e intente nuevamente",
              type: "error",
              confirmButtonClass: 'btn btn-danger',
              confirmButtonText: 'Salir'
          });        

    @endif
  </script>
@endsection
