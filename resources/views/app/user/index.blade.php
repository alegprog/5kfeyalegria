@extends('layouts.app')

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

<script>

    $(document).ready(function () {

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.trigger('confirm');
                //alert('enviando');

            }

        });

        $(document).on('confirm', function (e) {

            var ele = e.target;

            //e.preventDefault();

            //return ;

            window.location=ele.href;


        });
      });
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
            <li class="active">Usuario</li>
          </ol>
        </div>
        <div class="col-md-12 p-b-10">
          {!! link_to_route('register', trans('action.create'), $parameters = [], ['class'=>'btn btn-primary btn-l pull-right']) !!}
          <br><br>
        </div>
        <div class="col-md-12">
          @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><i class="fa fa-check"></i> {{trans('message.success')}}</strong> {{ session('success') }}
            </div>
          @endif
            <div class="panel panel-default">

                <div class="panel-heading"><i class="fa fa-list-alt"></i> {{trans('module.user')}}</div>

                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                     <tr>
                      <th>
                        {{ trans('label.name') }}
                      </th>
                      <th>
                        {{ trans('label.email') }}
                      </th>
                      <th>
                        {{ trans('label.type') }}
                      </th>
                      <th>
                        {{ trans('label.status') }}
                      </th>
                      <th class="width-100" >
                        {{ trans('label.action') }}
                      </th>
                     </tr>
                    </thead>
                    <tbody>
                      @foreach ($users as $user)
                      <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                          @if($user->type=='manager')
                            <label class="label label-primary">Administrador</label>
                          @elseif($user->type=='operator')
                            <label class=" label label-danger">Operador</label>
                          @endif
                        </td>
                        <td>
                          @if($user->deleted_at==Null)
                            <label class="label label-success">{{trans('label.active')}}</label>
                          @else
                            <label class=" label label-warning">{{trans('label.disable')}}</label>
                          @endif
                        </td>
                        <td>
                        @if($user->deleted_at==Null)
                          <a href="{{route('user.disable',['id'=>$user->id])}}" data-tr="tr_{{$user->id}}"                
                                   data-toggle="confirmation"

                                   data-btn-ok-label="{{trans('action.disable')}}" data-btn-ok-icon="fa fa-ban"

                                   data-btn-ok-class="btn btn-sm btn-warning m-r-10"

                                   data-btn-cancel-label="Cancelar"

                                   data-btn-cancel-icon="fa fa-chevron-circle-left"

                                   data-btn-cancel-class="btn btn-sm btn-default"

                                   data-title="¿Seguro que quiere deshabilitar?"

                                   data-placement="top" data-singleton="true"

                                   class="btn btn-warning btn-xs"

                                   ><i class="fa fa-ban"></i></a>
                          
                          @else

                            <a href="{{route('user.active',['id'=>$user->id])}}" data-tr="tr_{{$user->id}}"                
                                   data-toggle="confirmation"

                                   data-btn-ok-label="{{trans('action.active')}}" data-btn-ok-icon="fa fa-check"

                                   data-btn-ok-class="btn btn-sm btn-success m-r-10"

                                   data-btn-cancel-label="Cancelar"

                                   data-btn-cancel-icon="fa fa-chevron-circle-left"

                                   data-btn-cancel-class="btn btn-sm btn-default"

                                   data-title="¿Seguro que quiere activar?"

                                   data-placement="top" data-singleton="true"

                                   class="btn btn-success btn-xs"

                                   ><i class="fa fa-check"></i></a>

                           @endif
                          <!-- Single button -->
                          {{--<div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="fa fa-cog"></span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              @if($user->deleted_at==Null)
                              <li>
                                {!! link_to_route('user.disable', trans('action.disable'), ['id'=>$user->id],[]) !!}                                
                              </li>
                              @else
                              <li>
                                {!! link_to_route('user.active', trans('action.active'), ['id'=>$user->id],[]) !!}
                              </li>
                              @endif
                              <li>
                                
                              </li>
                            </ul>
                          </div>--}}
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {{ $users->links() }}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection