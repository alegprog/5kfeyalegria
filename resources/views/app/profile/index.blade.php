@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">

    <div class="col-md-12">
      <ol class="breadcrumb">
        <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
        <li class="active">Perfil</li>
      </ol>
    </div>

    <div class="col-md-12">
      @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><i class="fa fa-check"></i> {{trans('message.success')}}</strong> {{ session('success') }}
        </div>
      @endif
    </div>


        <div class="col-sm-5 col-sm-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-address-card-o" aria-hidden="true"></i>
 {{trans('action.update')}} {{trans('module.profile')}}</div>

                <div class="panel-body">
                  <form class="form-horizontal">
                  {{-- {!! Form::model($user,['route' => 'profile.update','class'=>'form-horizontal']) !!} --}}
                      {{ method_field('PUT') }}

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          {!! Form::label('name', trans('label.name'), ['class' => 'col-md-12']) !!}

                          <div class="col-md-12">
                            {!! Form::text('name', $user->name ,['class' => 'form-control', 'autofocus'=>'autofocus', 'disabled'=>'disabled']) !!}

                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>

                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          {!! Form::label('name', trans('label.email'), ['class' => 'col-md-12']) !!}

                          <div class="col-md-12">
                            {!! Form::text('email', $user->email,['class' => 'form-control', 'autofocus'=>'autofocus', 'disabled' => 'disabled']) !!}

                              @if ($errors->has('email'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>

                      <div class="form-group">
                          <div class="col-md-6">
                            <br><br>
                            {{--
                              {!! Form::button(trans('action.update'),['class'=>'btn btn-success btn-l','type'=>'submit']) !!}  
                            --}}
                          </div>
                      </div>
                      </form>
                 {{-- {!! Form::close() !!} --}}
                </div>
            </div>
        </div>


        <div class="col-sm-5 ">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-lock" aria-hidden="true"></i>
 {{trans('action.update')}} {{trans('label.password')}}</div>

                <div class="panel-body">
                  {!! Form::open(['route' => 'profile.update.password','class'=>'form-horizontal']) !!}
                      {{ method_field('PUT') }}

                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <label for="password" class="col-md-12">{{trans('label.password')}}</label>

                          <div class="col-md-12">
                              <input id="password" type="password" class="form-control" name="password" >

                              @if ($errors->has('password'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <label for="password-confirm" class="col-md-12">{{trans('label.password_confirmation')}}</label>

                          <div class="col-md-12">
                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6">
                              {!! Form::button(trans('action.update'),['class'=>'btn btn-success btn-l','type'=>'submit']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>


    </div>
</div>
@endsection
