@extends('layouts.app')

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

<script>

    $(document).ready(function () {

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.trigger('confirm');
                //alert('enviando');

            }

        });

        $(document).on('confirm', function (e) {

            var ele = e.target;

            //e.preventDefault();

            //return ;

            window.location=ele.href;


        });
      });
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
            <li class="active">Patrocinante</li>
          </ol>
        </div>
        <div class="col-md-12 p-b-10">
          {!! link_to_route('sponsor.create', trans('action.create'), $parameters = [], ['class'=>'btn btn-primary btn-l pull-right']) !!}
          <br><br>
        </div>
        <div class="col-md-12">
          @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><i class="fa fa-check"></i> {{trans('message.success')}}</strong> {{ session('success') }}
            </div>
          @endif
            <div class="panel panel-default">

                <div class="panel-heading"><i class="fa fa-list-alt"></i> {{trans('module.sponsors')}}</div>

                <div class="panel-body">
                  <table class="table table-striped">
                    <thead>
                     <tr>
                      <th>
                        {{ trans('label.name') }}
                      </th>                      
                      <th class="width-100" style="width:100px" >
                        {{ trans('label.action') }}
                      </th>
                     </tr>
                    </thead>
                    <tbody>
                      @foreach ($sponsors as $sponsor)
                      <tr>
                        <td>{{$sponsor->name}}</td>                        
                        <td>
                          <!-- Single button -->
                          <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="fa fa-cog"></span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              <li>
                                {!! link_to_route('sponsor.edit', trans('action.edit'), ['id'=>$sponsor->id],[]) !!}
                              </li>
                              <li>
                                <a href="{{route('sponsor.remove',['id'=>$sponsor->id])}}"
                                > Eliminar</a>
                              </li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {{ $sponsors->links() }}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection