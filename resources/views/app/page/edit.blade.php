@extends('layouts.app')

@section('style')
  {!! Html::style('assets/vendors/summernote/summernote.css') !!} 
@endsection

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>          
          <li class="active">Página a editar</li>
        </ol>
      </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-{{$icon}}" aria-hidden="true"></i>
                    {{trans('action.edit')}} {{$title}}</div>

                <div class="panel-body">

                  {!!  Form::model($page, ['route' => ['page.update', $page->id], 'class'=>'form-horizontal']) !!}
                      {{ method_field('PUT') }}
                      {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                          {!! Form::label('content', trans('label.content'), ['class' => 'col-md-12']) !!}
                    </div> 


                	<textarea name="content" id="summernote">{{$page->content}}</textarea>

                	<hr>

                	<div class="form-group">
                          <div class="col-md-2">
                              {!! Form::hidden('id', null) !!}
                              {!! Form::button(trans('action.update'),['class'=>'btn btn-success btn-l','type'=>'submit']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
  {!! Html::script('assets/vendors/summernote/summernote.min.js') !!} 
  {!! Html::script('assets/vendors/summernote/lang/summernote-es-Es.js') !!}

  <script type="text/javascript">
  	$(document).ready(function() {
	  $('#summernote').summernote({
	    lang: 'es-ES', // default: 'es-ES'
	    minHeight: 100, 
  		maxHeight: null, 
	  });	  
	});	
  </script>  
@endsection
