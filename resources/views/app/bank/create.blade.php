@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
          <li><a href="{{route('bank')}}">Banco</a></li>
          <li class="active">Agregar</li>
        </ol>
      </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus"></i> {{trans('action.create')}} {{trans('module.bank')}}</div>

                <div class="panel-body">
                  {!! Form::open(['route' => 'bank.store','class'=>'form-horizontal']) !!}
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          {!! Form::label('name', trans('label.name'), ['class' => 'col-md-4 control-label']) !!}

                          <div class="col-md-6">
                            {!! Form::text('name', null,['class' => 'form-control', 'autofocus'=>'autofocus']) !!}

                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>                     


                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              {!! Form::button(trans('action.save'),['class'=>'btn btn-success btn-l','type'=>'submit']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection