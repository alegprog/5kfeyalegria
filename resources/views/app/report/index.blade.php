@extends('layouts.app')

@section('style')
{!! Html::style('assets/vendors/sweetalert/sweetalert.min.css') !!} 
<style type="text/css">
  .table-responsive {
    width: 100%;
    margin-bottom: 16.5px;
    overflow-y: hidden;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    border: 1px solid #ddd;
  }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
            <li class="active">Reporte</li>
          </ol>
        </div>
        <div class="col-md-12 p-b-10">
          
          <br><br>
        </div>
        <div class="col-md-12">
          @if (session('success'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><i class="fa fa-check"></i> {{trans('message.success')}}</strong> {{ session('success') }}
            </div>
          @endif
            <div class="panel panel-default">

                <div class="panel-heading"><i class="fa fa-list-alt"></i> {{trans('module.report')}}</div>

                <div class="panel-body">

                  {!! Form::open(['route' => 'report.search','class'=>'form-horizontal', 'id'=> 'frmSearch']) !!}
                      {{ csrf_field() }}

                   <div class="row">

                     <div class="col-sm-4 col-md-3">

                      <div class="form-group{{ $errors->has('date_init') ? ' has-error' : '' }}">
                          {!! Form::label('date_init', trans('label.date_init'), ['class' => 'col-md-12']) !!}

                          <div class="col-md-12">
                            {!! Form::text('date_init', null,['class' => 'form-control input-sm datemask', 'autofocus'=>'autofocus', 'placeholder'=>'dd-mm-yyyy', 'data-mask'=>'']) !!}

                              @if ($errors->has('date_init'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('date_init') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>  

                     </div>

                     <div class="col-sm-4 col-md-3">

                      <div class="form-group{{ $errors->has('date_end') ? ' has-error' : '' }}">
                          {!! Form::label('date_end', trans('label.date_end'), ['class' => 'col-md-12']) !!}

                          <div class="col-md-12">
                            {!! Form::text('date_end', null,['class' => 'form-control input-sm datemask', 'autofocus'=>'autofocus', 'placeholder'=>'dd-mm-yyyy',  'data-mask'=>'']) !!}

                              @if ($errors->has('date_end'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('date_end') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>  

                     </div>

                     <div class="clearfix"></div>

                     <div class="form-group">
                        <div class="col-xs-12 text-right" >
                            <button class="btn btn-primary" style="margin-right: 15px;" type="submit">
                              <i class="fa fa-search"></i> Buscar </i>
                            </button>
                        </div>
                      </div>

                   </div>

                   <br>

                  {!! Form::close() !!}

                 @if($type_page=='report')

                 @if(count($inscriptions)<1)
                  <p class="alert alert-warning">No se ha encontrado ningun registro asociado.</p>
                 @else

                 <div class="table-responsive" style="padding:10px;">                    

                  <table id="table_report" class="table table-striped">
                    <thead>
                     <tr>
                      <th>
                        {{ trans('label.code') }}
                      </th>
                      <th>
                        {{ trans('label.date') }}
                      </th> 
                      <th>
                        {{ trans('label.identification') }}
                      </th> 
                      <th style="min-width: 150px;">
                        {{ trans('label.name') }}
                      </th>  
                      <th>
                        {{ trans('label.gender') }}
                      </th> 
                      <th>
                        {{ trans('label.age') }}
                      </th>                       
                      <th>
                        {{ trans('label.size') }}
                      </th> 
                      <th style="min-width: 150px;">
                        {{ trans('label.email') }}
                      </th> 
                      <th style="min-width: 100px;">
                        {{ trans('label.category') }}
                      </th>
                      <th style="min-width: 120px;">
                        {{ trans('label.payment_method') }}
                      </th>
                      <th style="min-width: 120px;">
                        {{ trans('label.bank') }}
                      </th> 
                      <th>
                        {{ trans('label.number') }}
                      </th>   
                      <th>
                        {{ trans('label.status') }}
                      </th> 
                      <th style="min-width: 100px;">
                        Retiro de kits
                      </th> 
                      <th style="min-width: 60px;">
                        {{ trans('label.type') }}
                      </th>  
                      <th style="min-width: 120px;">
                        {{ trans('label.registered_by') }}
                      </th>               
                      
                     </tr>
                    </thead>
                    <tbody>
                      @forelse ($inscriptions as $inscription)
                      <tr>
                        <td>{{str_pad($inscription->id,7,'0',STR_PAD_LEFT)}}</td>
                        <td>{{$inscription->created_at_es}}</td>
                        <td>{{$inscription->competitors->first()->identification}}</td>
                        <td>{{$inscription->competitors->first()->name}}</td>                        
                        <td>{{$inscription->competitors->first()->size->name}}</td>
                        <td>{{$inscription->competitors->first()->age}}</td>
                        <td>{{$inscription->competitors->first()->gender}}</td>
                        <td>{{$inscription->competitors->first()->email}}</td>
                        <td>{{$inscription->competitors->first()->category}}</td>
                        <td>{{$inscription->payments ? $inscription->payments->first()->method :''}}</td>
                        <td>{{$inscription->payments ? $inscription->payments->first()->bank ? $inscription->payments->first()->bank->name : '' : ''}}</td>
                        <td>{{$inscription->payments->first() ? $inscription->payments->first()->number : ''}}</td>
                        <td>{{$inscription->status_es}}</td>
                        <td>
                          @if($inscription->kits)
                            SI
                          @elseif(!$inscription->kits)
                            NO
                          @endif
                        </td>
                        <td>{{$inscription->type_es}}</td>
                        <td>{{$inscription->registered_by}}</td>
                      </tr>
                      @empty
                        <tr>
                         <td colspan="15">
                          <p class="alert alert-warning">No se ha encontrado ningun registro asociado.</p>
                         </td>
                        </tr>
                      @endforelse
                    </tbody>
                  </table>
                  <br>
                  
                 
                 </div>
                   @endif
                   
                 @endif

                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('script')  
  {!! Html::script('assets/vendors/sweetalert/sweetalert.min.js') !!}  
  {!! Html::script('assets/vendors/inputmask/inputmask.js') !!}
  {!! Html::script('assets/vendors/inputmask/jquery.inputmask.js') !!}
  {!! Html::script('assets/vendors/inputmask/inputmask.date.extensions.js') !!}
  {!! Html::script('assets/vendors/inputmask/inputmask.extensions.js') !!}
  {!! Html::script('assets/vendors/tableexport/xlsx.core.min.js') !!}
  {!! Html::script('assets/vendors/tableexport/Blob.min.js') !!}
  {!! Html::script('assets/vendors/tableexport/FileSaver.min.js') !!}
  {!! Html::script('assets/vendors/tableexport/tableexport.min.js') !!}

  <script>

     @if($errors->any())
        
          swal({
              title: "Error",
              text: "Los datos introducidos son incorrecto por verifique e intente nuevamente",
              type: "error",
              confirmButtonClass: 'btn btn-danger',
              confirmButtonText: 'Salir'
          });        

      @endif
    
    //Datemask dd/mm/yyyy
      $(".datemask").inputmask("dd-mm-yyyy", {
          "placeholder": "dd-mm-yyyy"
      });

      /* default charset encoding (UTF-8) */
      $.fn.tableExport.charset = "charset=utf-8";

      /* default class to style buttons when not using bootstrap  */
      $.fn.tableExport.defaultButton = "button-default";

      /* bootstrap classes used to style and position the export buttons */
      $.fn.tableExport.bootstrap = ["btn", "btn-success", "btn-toolbar"];


      
      (function ($, window) {

          new TableExport($('table'), {formats: ['csv'], fileName: "report-inscripcion", bootstrap: true})

      }).call(this, jQuery, window);

      
      

</script>

@endsection
