@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
          <li><a href="{{route('size')}}">Talla</a></li>
          <li class="active">Editar</li>
        </ol>
      </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    {{trans('action.edit')}} {{trans('module.size')}}</div>

                <div class="panel-body">

                  {!!  Form::model($size, ['route' => ['size.update', $size->id], 'class'=>'form-horizontal']) !!}
                      {{ method_field('PUT') }}
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          {!! Form::label('name', trans('label.name'), ['class' => 'col-md-4 control-label']) !!}

                          <div class="col-md-6">
                              {!! Form::text('name', null,['class'=>'form-control', 'autofocus'=>'autofocus']) !!}

                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>                     


                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              {!! Form::hidden('id', null) !!}
                              {!! Form::button(trans('action.update'),['class'=>'btn btn-success btn-l','type'=>'submit']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection