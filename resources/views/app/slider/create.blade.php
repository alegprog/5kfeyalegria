@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="{{route('home')}}"> <i class="fa fa-tachometer"></i> Dashboard</a></li>
          <li><a href="{{route('slider')}}">Slider</a></li>
          <li class="active">Agregar</li>
        </ol>
      </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-plus"></i> {{trans('action.create')}} {{trans('module.slider')}}</div>

                <div class="panel-body">
                  {!! Form::open(['route' => 'slider.store','class'=>'form-horizontal','enctype' => 'multipart/form-data']) !!}
                      {{ csrf_field() }}

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          {!! Form::label('name', trans('label.name'), ['class' => 'col-sm-3 control-label']) !!}

                          <div class="col-md-7">
                            {!! Form::text('name', null,['class' => 'form-control', 'autofocus'=>'autofocus']) !!}

                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                          </div>

                      </div>  

                      

                        <div class="form-group{{ $errors->has('banner') ? ' has-error' : '' }}">
                          {!! Form::label('banner', trans('label.banner'), ['class' => 'col-sm-3 control-label']) !!}

                          <div class="col-md-7">
                            {!! Form::file('banner',['id'=>'banner','class' => 'form-control', 'tabindex'=>'2']) !!}

                              @if ($errors->has('banner'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('banner') }}</strong>
                                  </span>
                              @endif
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-xs-8 col-sm-offset-3 col-sm-7 col-md-7">
                            <img src="{{ asset('assets/image/default/default-thumbnail.jpg') }}" style="width:100%;" id="image-preview" class="img-responsive" alt="Imagen del Producto" />
                          </div>
                        </div>

                        {{--<input type="file" name="image" id="image">--}}


                                      


                      <div class="form-group">
                          <div class="col-md-6 col-sm-offset-3">
                              {!! Form::button(trans('action.save'),['class'=>'btn btn-success btn-l','type'=>'submit']) !!}
                          </div>
                      </div>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
  <script>
  function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
   }
   $("#banner").change(function(){
      console.log(this);
      readURL(this);
   });
  </script>
@endsection