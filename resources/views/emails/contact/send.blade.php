
@component('mail::message')

# Mensaje de {{ $contact->name }}
Enviado desde 5kporlaeducacion.com

# Información
@component('mail::panel')
## {{trans('label.email')}}
{{ $contact->email }}
## {{trans('label.phone')}}
{{ $contact->phone }}
## {{trans('label.message')}}
{{ $contact->message }}
@endcomponent

{{trans('message.regards')}},<br>
{{ config('app.name') }}
@endcomponent
