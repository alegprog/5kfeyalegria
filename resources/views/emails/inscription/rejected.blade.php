@component('mail::message')

# Hola, tu inscripción fue RECHAZADA.
La solicitud pudo ser rechazada por los siquiente motivos.
+ La información suministrada es incorrecta.

# Motivo de rechazo
@component('mail::panel')
"{{$inscription->comment ? $inscription->comment : 'Se han verificado los datos registrado y son incorrecto'}}"
@endcomponent

# Información de registro
@component('mail::panel')
## Código de inscripción
{{str_pad($inscription->id,7,"0",STR_PAD_LEFT)}}

@endcomponent

Para cualquier duda puede dirigise nuestra pagina de [ayuda]({{ $urlHelp }}) o contactarnos por nuestros [formulario]({{ $urlContact }})

{{trans('message.regards')}},<br>
info@sillaroja5k.com.<br>
@endcomponent