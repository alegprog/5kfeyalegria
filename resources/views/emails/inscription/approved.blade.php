@component('mail::message')

# Hola, tu inscripción fue APROBADA con éxito.
Ya eres un participante oficial de la carrera 5K por la educación. 

"Gracias por participar y apoyar la educación para los más necesitados. Hay miles de niños y niñas en todo el ECUADOR que necesitan de tu solidaridad para seguir estudiando".

Por favor imprime el ticket adjunto a este correo para que puedas retirar tu kit en nuestras oficina.

Fecha y hora del evento: 21 de enero de 2018 hora 8:00 am. 

# Información de registro
@component('mail::panel')
## Numero del participante
{{str_pad($inscription->id,7,"0",STR_PAD_LEFT)}}

## Identificación
{{$inscription->competitors->first()->identification}}

## Nombre y apellido
{{$inscription->competitors->first()->name}}

## Categoría
{{$inscription->competitors->first()->category}}

@component('mail::button', ['url' => $actionUrl, 'color' => 'blue'])
Ver inscripción
@endcomponent
@endcomponent

Para cualquier duda puede dirigirte a nuestra pagina de [ayuda]({{ $urlHelp }}) o contactarnos por nuestros [formulario]({{ $urlContact }})

{{trans('message.regards')}},<br>
info@sillaroja5k.com.<br>
@endcomponent