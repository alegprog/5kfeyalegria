@component('mail::message')

### Hola, se te ha enviado el siguiente un comentario desde sillaroja5k.com.

# Comentario
@component('mail::panel')
"{{$inscription->comment ? $inscription->comment : ''}}"
@endcomponent

# Información de registro
@component('mail::panel')
## Código de inscripción
{{str_pad($inscription->id,7,"0",STR_PAD_LEFT)}}

@endcomponent

Para cualquier duda puede dirigise nuestra pagina de [ayuda]({{ $urlHelp }}) o contactarnos por nuestros [formulario]({{ $urlContact }})

{{trans('message.regards')}},<br>
info@sillaroja5k.com.<br>
@endcomponent
