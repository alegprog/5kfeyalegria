
@component('mail::message')

# Hola, tu registro fue exitoso.
Gracias por registrarte en la carrera 5K por la educación.

Tu registro será verificado y confirmado en las próximas horas recibirás un correo con su aprobación.

@component('mail::panel')
"Este registro no garantiza su participación hasta que su pago sea confirmado".
@endcomponent

# Información de registro
@component('mail::panel')
## Código de inscripción
{{str_pad($inscription->id,7,"0",STR_PAD_LEFT)}}

## Metodo de pago
{{$inscription->payments->first() ? $inscription->payments->first()->method : ''}}

@component('mail::button', ['url' => $actionUrl, 'color' => 'blue'])
Ver inscripción
@endcomponent
@endcomponent

{{trans('message.regards')}},<br>
info@sillaroja5k.com.<br>
@endcomponent