@extends('layouts.main')

@section('style')
  {!! Html::style('assets/vendors/sweetalert/sweetalert.min.css') !!} 
@endsection

@section('content')

@php

 $next_competitor=1;
 $next_payment=1;

 if(old('competitors') && count(old('competitors')>0)){
  foreach (old('competitors') as $key => $value){
    if($key>$next_competitor){
       $next_competitor=$key;
    }
  }
 }

 //dd(old(),$errors);

@endphp
<!--<div class="row">-->
    <div class="bg-gray m-b-30">
      <div class="container">  
      <div class="col-md-12">
        <ol class="breadcrumb">          
          <li><a href="{{route('front.index')}}">Inicio</a></li>
          <li class="active">Inscribirme</li>
        </ol>
      </div>
      </div>
    </div>
<div class="container">   
    <div class="row">

        <div class="col-md-12">
        @if (session('danger'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><i class="fa fa-check"></i> {{trans('message.danger')}}</strong> {{ session('danger') }}
            </div>
          @endif

            <header class="section-header"><span class="f-14">por favor completa todo los datos solicitados</span> <h2 class="f-20"><i class="fa fa-user-plus f-20"></i> Inscribirme</h2></header>

            <div class="card">
                <!--<div class="card-header"><i class="fa fa-user-plus f-20"></i> Inscribirme</div>-->

                <div class="card-block">
                  <p class="text-right f-16">
                  	<b><a href="{{url('help')}}"> ¿Como inscribirme? <i class="fa fa-question-circle f-20"></i></a></b>
                  </p>
                  {!! Form::open(['route' => 'front.inscription.store','id'=>'frmInscription','class'=>'form-horizontal']) !!}
                      {{ csrf_field() }}

                     @include('front.inscription.partials.competitor')  

                     <div class="col-md-12">
                        <hr>
                     </div>  

                     @include('front.inscription.partials.payment')   

                     <div class="col-md-12">

                       <div class="checkbox ">
                          <label>
                            {!!Form::checkbox('terms', true)!!}
                             &nbsp;&nbsp;Aceptar <a href="{{url('terms')}}" target="_blank">terminos & condiciones</a>
                          </label>
                          <div class="form-group{{ $errors->has('terms') ? ' has-error' : '' }}">
                            @if ($errors->has('terms'))
                              <span class="help-block">
                                <strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $errors->first('terms') }}</strong>
                              </span>
                            @endif
                          </div>
                       </div>

                     </div>           


                      <div class="form-group">
                          <div class="col-md-12">
                              <hr>
                              <i class="fa check f-20"></i>
                              <button class="btn btn-success btn-l" type="submit"

	                            data-toggle="confirmation"

	                            data-btn-ok-href="javascript:void(0)"

	                            data-btn-ok-label="Aceptar" data-btn-ok-icon="fa fa-check-square-o"

	                            data-btn-ok-class="btn btn-sm btn-success m-r-10"

	                            data-btn-cancel-label="Cancelar"

	                            data-btn-cancel-icon="fa fa-chevron-circle-left"

	                            data-btn-cancel-class="btn btn-sm btn-default"

	                            data-title="¿Está seguro que todo los datos estan correctamente?"

	                            data-placement="top" data-singleton="true"
                              >
					          	<i class="fa fa-check"></i> Inscribirme 
					          </button>
                          </div>
                      </div>
                  {!! Form::close() !!}
                </div>
            </div>
        </div>
      </div>
    </div>
    @include('pages.sponsor')
    <div class="p-t-40 p-b-30 question-footer">
      <div class="container">  
        <div class="col-md-8"><h3 class="text-center f-20 text-semibold p-t-m-t-0" 
        >¿Quieres conocer más sobre la carrera 5k por la educación?</h3></div> <div class="col-md-4 text-center p-t-20"><a href="{{url('/')}}" class="btn btn-default btn-l text-uppercase"><i class="fa fa-home"></i> Ir al inicio</a></div>
      </div>
    </div>
    
<!--</div>-->
@endsection

@section('modal')
  <div class="modal fade" id="progress" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title f-20" id="myModalLabel">Procesando inscripción...</h4>
          </div>
          <div class="modal-body">
            <div class="progress">
              <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('script')  

  {!! Html::script('assets/vendors/sweetalert/sweetalert.min.js') !!}    
  {!! Html::script('assets/vendors/inputmask/inputmask.js') !!}
  {!! Html::script('assets/vendors/inputmask/jquery.inputmask.js') !!}
  {!! Html::script('assets/vendors/inputmask/inputmask.date.extensions.js') !!}
  {!! Html::script('assets/vendors/inputmask/inputmask.extensions.js') !!}
  {!! Html::script('assets/vendors/jquery_numeric/jquery.numeric.js') !!}
  {!! Html::script('assets/vendors/bootstrap-confirmation/bootstrap-confirmation.min.js') !!} 


  @include('sweet::alert')
  <script>


      @if($errors->any())
        
          swal({
              title: "Error",
              text: "Los datos introducidos son incorrecto por verifique e intente nuevamente",
              type: "error",
              confirmButtonClass: 'btn btn-danger',
              confirmButtonText: 'Salir'
          });        

      @endif
    
    //Datemask dd/mm/yyyy
      $(".datemask").inputmask("dd/mm/yyyy", {
          "placeholder": "dd-mm-yyyy"
      });
      //Datemask2 mm/dd/yyyy
      $(".datemask2").inputmask("mm/dd/yyyy", {
          "placeholder": "mm/dd/yyyy"
      });
      //Money Euro
      //$("[data-mask]").inputmask();

      {{--$('.amount').numeric({ decimal : ".", negative : false, decimalPlaces : 2 });--}}
      $('.number').numeric({ decimal : false, negative : false});
    

    $(document).ready(function(){
      var next_competitor = {{$next_competitor}}+1;
      var next_payment = {{$next_payment}}+1;  

      $(".add-competitor").click(function(){
        var html = $templateCompetitor
        .replace(/:next:/g, next_competitor);
        $(".after-add-competitor").prepend(html);
        //alert(html);
        $(".datemask").inputmask("dd-mm-yyyy", {
          "placeholder": "dd-mm-yyyy"
        });
        $("#competitor_"+next_competitor+"_identification").focus();
        next_competitor = next_competitor + 1;
      });

      $(".add-payment").click(function(){
        var html = $templatePayment
        .replace(/:next:/g, next_payment);
        $(".after-add-payment").prepend(html);
        //alert(html);
        $('.amount').numeric({ decimal : ",", negative : false, decimalPlaces : 2 });
        $('.number').numeric({ decimal : false, negative : false});
        $("#payment_"+next_payment+"_payment_method").focus();
        next_payment = next_payment + 1;
      });

      $("body").on("click",".remove",function(){ 

          $(this).parents(".control-panel").remove();

      });

      $("body").on("change",".payment_method",function(){           

          var payment_method=$(this).data('id');
          if($(this).val()=='office'){
            $('.payments_'+payment_method+'_number').val('');
            $('.payments_'+payment_method+'_number').prop('disabled','true');
            $('.payments_'+payment_method+'_bank').prop('disabled','true');
          }else{
            $('.payments_'+payment_method+'_number').removeAttr('disabled');
            $('.payments_'+payment_method+'_bank').removeAttr('disabled');
          }
          
          console.log(payment_method,$(this).val());

      });

      

    });
  </script>

  <script>
    $(document).ready(function () {

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.trigger('confirm');
                //alert('enviando');

            }

        });

        $(document).on('confirm', function (e) {

            //var ele = e.target;

            //e.preventDefault();

            //return true ;

            $('#progress').modal('show');

            $( "#frmInscription" ).submit();            

            //window.location=ele.href;


        });
      });
  </script>

  


@endsection
