
<div class="row">

  <div class="col-md-12">

  @if ($errors->has('payments'))
    <div class="alert alert-danger">
      <strong>{{ $errors->first('payments') }}</strong>
    </div>
 @endif

  <h5 class="title f-16"><i class="fa fa-money f-18"></i> &nbsp;Pagos</h5>

  <!-- Init card -->
  <div class="card card-accent-warning">
    <div class="card-header">
      <span class=""><i class="fa fa-dollar"></i> Datos del Pago</span> 
      <!--<span class="pull-right" ><i class="fa fa-remove text-danger"></i></span>-->
    </div>
    <!-- Init block -->
    <div class="card-block">

    <!-- Init Input PaymentMethod -->
  	  <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('payments.1.payment_method') ? ' has-error' : '' }}">
              {!! Form::label('payment_method', trans('label.payment_method'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">                
                {!! Form::select('payments[1][payment_method]', array_pluck($payment_method,'name','id'), null, ['class' => 'form-control input-sm payment_method', 'placeholder' => '-- Selecione --','data-id'=>'1']) !!}

                  @if ($errors->has('payments.1.payment_method'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payments.1.payment_method') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>  
       <!-- End Input PaymentMethod --> 

       <!-- Init Input Amount -->
       {{--<div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has('payments.1.amount') ? ' has-error' : '' }}">
              {!! Form::label('amount', trans('label.amount'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('payments[1][amount]', null,['class' => 'form-control input-sm amount', 'placeholder'=>'100.00']) !!}

                  @if ($errors->has('payments.1.amount'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payments.1.amount') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>--}}
       <!-- End Input Amount --> 

       <!-- Init Input Number -->
       <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('payments.1.number') ? ' has-error' : '' }}">
              {!! Form::label('number', trans('label.number'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('payments[1][number]', null,['class' => 'form-control input-sm number payments_1_number',  'placeholder'=>'784026394']) !!}

                  @if ($errors->has('payments.1.number'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payments.1.number') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>
       <!-- End Input Number --> 


        <!-- Init Input Bank -->
       <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('payments.1.bank') ? ' has-error' : '' }}">
              {!! Form::label('bank', trans('label.bank'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">                
                {!! Form::select('payments[1][bank]', array_pluck($banks,'name','id'), null, ['placeholder' => '-- Selecione --','class'=>'form-control select-basic input-sm payments_1_bank']) !!}

                  @if ($errors->has('payments.1.bank'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payments.1.bank') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

      </div>
      <!-- End Input Bank --> 

    </div>
    <!-- End block -->
	
  </div>
  <!-- End card -->

  
  <div class="col-md-12 after-add-payment" >

    @php
    $list_payments=[];
    @endphp
    @if(old('payments') && count(old('payments')>0))
    @php
      $list_payments=old('payments'); 

    @endphp
    @endif

    @foreach ($list_payments as $key => $payment)
     @if($key>1)
      @php
        $payment_input_method = old("payments.$key.payment_method");
        $payment_amount = old("payments.$key.payment_amount");
        $payment_number = old("payments.$key.payment_number");
        $payment_bank = old("payments.$key.payment_bank");        
      @endphp

      <!-- Init card -->
  <div class="row control-panel"><div class="card card-accent-warning">
    <div class="card-header">
      <span class=""><i class="fa fa-dollar"></i> Datos del Pago</span> <span class="pull-right" ><i class="remove fa fa-remove text-danger"></i></span>
    </div>
    <!-- Init block -->
    <div class="card-block">

    <!-- Init Input PaymentMethod -->
      <div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has("payments.$key.payment_method") ? ' has-error' : '' }}">
              {!! Form::label('payment_method', trans('label.payment_method'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">                
                {!! Form::select("payments[$key][payment_method]", array_pluck($payment_method,'name','id'), $payment_input_method, ['class' => 'form-control input-sm', 'placeholder' => '-- Selecione --']) !!}

                  @if ($errors->has("payments.$key.payment_method"))
                      <span class="help-block">
                          <strong>{{ $errors->first("payments.$key.payment_method") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>  
       <!-- End Input PaymentMethod --> 

       <!-- Init Input Amount -->
       {{--<div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has("payments.$key.amount") ? ' has-error' : '' }}">
              {!! Form::label('amount', trans('label.amount'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text("payments[$key][amount]", $payment_amount,['class' => 'form-control input-sm amount','placeholder'=>'100.00']) !!}

                  @if ($errors->has("payments.$key.amount"))
                      <span class="help-block">
                          <strong>{{ $errors->first("payments.$key.amount") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>--}}
       <!-- End Input Amount --> 

       <!-- Init Input Number -->
       <div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has("payments.$key.number") ? ' has-error' : '' }}">
              {!! Form::label('number', trans('label.number'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text("payments[$key][number]", $payment_number,['class' => 'form-control input-sm number',  'placeholder'=>'784026394']) !!}

                  @if ($errors->has("payments.$key.number"))
                      <span class="help-block">
                          <strong>{{ $errors->first("payments.$key.number") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>
       <!-- End Input Number --> 


        <!-- Init Input Bank -->
       <div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has("payments.$key.bank") ? ' has-error' : '' }}">
              {!! Form::label('bank', trans('label.bank'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">                
                {!! Form::select("payments[$key][bank]", array_pluck($banks,'name','id'), $payment_bank, ['placeholder' => '-- Selecione --','class'=>'form-control select-basic input-sm']) !!}

                  @if ($errors->has("payments.$key.bank"))
                      <span class="help-block">
                          <strong>{{ $errors->first("payments.$key.bank") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

      </div>
      <!-- End Input Bank --> 

    </div>
    <!-- End block -->
  
  </div></div>
  <!-- End card -->

     @endif
    @endforeach

  </div>

  {{--<div class="form-group">
    <div class="col-md-12 text-right">
        <button class="btn btn-warning add-payment" type="button">
          <i class="fa fa-plus"></i> {{trans('label.payment')}} <i class="fa fa-dollar"></i>
        </button>
    </div>
  </div>--}}

</div>

</div>

<script>
var $templatePayment=`<div class="row control-panel"><!-- Init card -->
  <div class="card card-accent-warning">
    <div class="card-header">
      <span class=""><i class="fa fa-dollar"></i> Datos del Pago</span> <span class="pull-right" ><i class="remove fa fa-remove text-danger"></i></span>
    </div>
    <!-- Init block -->
    <div class="card-block">

    <!-- Init Input PaymentMethod -->
      <div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has('payments.:next:.payment_method') ? ' has-error' : '' }}">
              {!! Form::label('payment_method', trans('label.payment_method'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">                
                {!! Form::select('payments[:next:][payment_method]', array_pluck($payment_method,'name','id'), null, ['class' => 'form-control input-sm', 'placeholder' => '-- Selecione --','id'=>'payments_:next:_payment_method']) !!}

                  @if ($errors->has('payments.:next:.payment_method'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payments.:next:.payment_method') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>  
       <!-- End Input PaymentMethod --> 

       <!-- Init Input Amount -->
       {{--<div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has('payments.:next:.amount') ? ' has-error' : '' }}">
              {!! Form::label('amount', trans('label.amount'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('payments[:next:][amount]', null,['class' => 'form-control input-sm amount', 'placeholder'=>'100.00']) !!}

                  @if ($errors->has('payments.:next:.amount'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payments.:next:.amount') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>--}}
       <!-- End Input Amount --> 

       <!-- Init Input Number -->
       <div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has('payments.:next:.number') ? ' has-error' : '' }}">
              {!! Form::label('number', trans('label.number'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('payments[:next:][number]', null,['class' => 'form-control input-sm number',  'placeholder'=>'784026394']) !!}

                  @if ($errors->has('payments.:next:.number'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payments.:next:.number') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       </div>
       <!-- End Input Number --> 


        <!-- Init Input Bank -->
       <div class="col-sm-6 col-md-3">

          <div class="form-group{{ $errors->has('payments.:next:.bank') ? ' has-error' : '' }}">
              {!! Form::label('bank', trans('label.bank'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">                
                {!! Form::select('payments[:next:][bank]', array_pluck($banks,'name','id'), null, ['placeholder' => '-- Selecione --','class'=>'form-control select-basic input-sm']) !!}

                  @if ($errors->has('payments.:next:.bank'))
                      <span class="help-block">
                          <strong>{{ $errors->first('payments.:next:.bank') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

      </div>
      <!-- End Input Bank --> 

    </div>
    <!-- End block -->
  
  </div>
  <!-- End card --></div>`;

</script>