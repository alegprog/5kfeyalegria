

<!-- Init row -->
<div class="row">

 <!-- Init col-md-12 -->
 <div class="col-md-12">

 @if ($errors->has("competitors"))
    <div class="alert alert-danger">
      <strong>{{ $errors->first("competitors") }}</strong>
    </div>
 @endif

 <h5 class="title f-16"><i class="fa fa-users f-18"></i> &nbsp;Participantes</h5>

  <!-- Init card -->
  <div class="card card-accent-primary">
    <div class="card-header">
       <span class=""><i class="fa fa-id-card-o f-18"></i> Datos del Participante</span> 
       <!--<span class="pull-right" ><i class="fa fa-remove text-danger"></i></span>-->

    </div>
    <!-- Init block -->
    <div class="card-block">

        <!-- Init Input Identificacion -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.1.identification') ? ' has-error' : '' }}">
              {!! Form::label('identification', trans('label.identification'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('competitors[1][identification]', null,['class' => 'form-control input-sm',  'placeholder'=> trans('label.identification') ]) !!}

                  @if ($errors->has('competitors.1.identification'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.1.identification') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>  
        <!-- End Input Identificacion -->

        <!-- Init Input Name -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.1.name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'Nombre y apellido', ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('competitors[1][name]', null,['class' => 'form-control input-sm',  'placeholder'=>'Nombre y apellido']) !!}

                  @if ($errors->has('competitors.1.name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.1.name') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

       	</div>
       	<!-- End Input Name -->

        <div class="visible-sm clearfix"></div>

        <!-- Init Input Email -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.1.email') ? ' has-error' : '' }}">
              {!! Form::label('email', trans('label.email'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('competitors[1][email]', null,['class' => 'form-control input-sm', 'placeholder'=>trans('label.email')]) !!}

                  @if ($errors->has('competitors.1.email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.1.email') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Email -->

        <div class="visible-md visible-lg clearfix"></div>

        <!-- Init Input Gender -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.1.gender') ? ' has-error' : '' }}">
              {!! Form::label('gender', trans('label.gender'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::select('competitors[1][gender]', ['M' => 'Masculino', 'F' => 'Femenino'], null, ['class' => 'form-control input-sm', 'placeholder' => '-- Selecione --']) !!}

                  @if ($errors->has('competitors.1.gender'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.1.gender') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div> 
        <!-- End Input Gender -->

        <div class="visible-sm clearfix"></div>

        <!-- Init Input Size -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.1.size') ? ' has-error' : '' }}">
              {!! Form::label('size', trans('label.size'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::select('competitors[1][size]', array_pluck($sizes,'name','id'), null, ['class' => 'form-control input-sm', 'placeholder' => '-- Selecione --']) !!}

                  @if ($errors->has('competitors.1.size'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.1.size') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Size -->  

        <!-- Init Input Birthdate -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.1.birthdate') ? ' has-error' : '' }}">
              {!! Form::label('birthdate', trans('label.birthdate'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('competitors[1][birthdate]', null,['class' => 'form-control input-sm datemask',  'placeholder'=>'dd-mm-yyyy', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'", 'data-mask'=>'']) !!}

                  @if ($errors->has('competitors.1.birthdate'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.1.birthdate') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Birthdate --> 

    </div>
    <!-- End block -->

  </div>  
  <!-- End card -->

  

 </div>
 <!-- End col-md-12 -->

 <div class="col-md-12 after-add-competitor" >

  @php
    $list_competitors=[];
  @endphp
  @if(old('competitors') && count(old('competitors')>0))
  @php
    $list_competitors=old('competitors'); 

  @endphp
  @endif

  @foreach ($list_competitors as $key => $competitor)
   @if($key>1)
    @php
      $competitor_name = old("competitors.$key.name");
      $competitor_identification= old("competitors.$key.identification");
      $competitor_email= old("competitors.$key.email");
      $competitor_gender= old("competitors.$key.gender");
      $competitor_size= old("competitors.$key.size");
      $competitor_birthdate= old("competitors.$key.birthdate");
    @endphp

    <!-- Init card -->
  <div class="card card-accent-primary control-panel">
    <div class="card-header">
       <span class=""><i class="fa fa-id-card-o f-18"></i> Datos del Participante</span> 
       <span class="pull-right" ><i class="remove fa fa-remove text-danger"></i></span>

    </div>
    <!-- Init block -->
    <div class="card-block">

        <!-- Init Input Identificacion -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has("competitors.$key.identification") ? ' has-error' : '' }}">
              {!! Form::label('identification', trans('label.identification'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text("competitors[$key][identification]", $competitor_identification,['class' => 'form-control input-sm', 'placeholder'=> trans('label.identification') ]) !!}

                  @if ($errors->has("competitors.$key.identification"))
                      <span class="help-block">
                          <strong>{{ $errors->first("competitors.$key.identification") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>  
        <!-- End Input Identificacion -->

        <!-- Init Input Name -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has("competitors.$key.name") ? ' has-error' : '' }}">
              {!! Form::label('name', 'Nombre y apellido', ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text("competitors[$key][name]", $competitor_name,['class' => 'form-control input-sm',  'placeholder'=>'Nombre y apellido']) !!}

                  @if ($errors->has("competitors.$key.name"))
                      <span class="help-block">
                          <strong>{{ $errors->first("competitors.$key.name") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Name -->

        <div class="visible-sm clearfix"></div>

        <!-- Init Input Email -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has("competitors.$key.email") ? ' has-error' : '' }}">
              {!! Form::label('email', trans('label.email'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text("competitors[$key][email]", $competitor_email,['class' => 'form-control input-sm',  'placeholder'=>trans('label.email')]) !!}

                  @if ($errors->has("competitors.$key.email"))
                      <span class="help-block">
                          <strong>{{ $errors->first("competitors.$key.email") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Email -->

        <div class="visible-md visible-lg clearfix"></div>

        <!-- Init Input Gender -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has("competitors.$key.gender") ? ' has-error' : '' }}">
              {!! Form::label('gender', trans('label.gender'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::select("competitors[$key][gender]", ['M' => 'Masculino', 'F' => 'Femenino'], $competitor_gender, ['class' => 'form-control input-sm', 'placeholder' => '-- Selecione --']) !!}

                  @if ($errors->has("competitors.$key.gender"))
                      <span class="help-block">
                          <strong>{{ $errors->first("competitors.$key.gender") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div> 
        <!-- End Input Gender -->

        <div class="visible-sm clearfix"></div>

        <!-- Init Input Size -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has("competitors.$key.size") ? ' has-error' : '' }}">
              {!! Form::label('size', trans('label.size'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::select("competitors[$key][size]", array_pluck($sizes,'name','id'), $competitor_size, ['class' => 'form-control input-sm', 'placeholder' => '-- Selecione --']) !!}

                  @if ($errors->has("competitors.$key.size"))
                      <span class="help-block">
                          <strong>{{ $errors->first("competitors.$key.size") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Size -->  

        <!-- Init Input Birthdate -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has("competitors.$key.birthdate") ? ' has-error' : '' }}">
              {!! Form::label('birthdate', trans('label.birthdate'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text("competitors[$key][birthdate]", $competitor_birthdate,['class' => 'form-control input-sm datemask', 'placeholder'=>'dd-mm-yyyy', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'", 'data-mask'=>'']) !!}

                  @if ($errors->has("competitors.$key.birthdate"))
                      <span class="help-block">
                          <strong>{{ $errors->first("competitors.$key.birthdate") }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Birthdate --> 

    </div>
    <!-- End block -->

  </div>  
  <!-- End card -->

    @endif
  @endforeach

  {{--<div class="form-group">
    <div class="col-md-12 text-right">
        <button class="btn btn-primary add-competitor" type="button">
          <i class="fa fa-plus"></i> {{trans('label.competitor')}} <i class="fa fa-users"></i>
        </button>
    </div>
  </div>--}}

 </div>



</div>
<!-- End row -->

<script>
var $templateCompetitor=`<div class="card card-accent-primary control-panel">
    <div class="card-header">
       <span class=""><i class="fa fa-id-card-o f-18"></i> Datos del Participante</span> <span class="pull-right" ><i class="remove fa fa-remove text-danger"></i></span>

    </div>
    <!-- Init block -->
    <div class="card-block">

        <!-- Init Input Identificacion -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.:next:.identification') ? ' has-error' : '' }}">
              {!! Form::label('identification', trans('label.identification'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('competitors[:next:][identification]', null,['class' => 'form-control input-sm',  'placeholder'=> trans('label.identification'),'id'=>'competitor_:next:_identification' ]) !!}

                  @if ($errors->has('competitors.:next:.identification'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.:next:.identification') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>  
        <!-- End Input Identificacion -->

        <!-- Init Input Name -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.:next:.name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'Nombre y apellido', ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('competitors[:next:][name]', null,['class' => 'form-control input-sm',  'placeholder'=>'Nombre y apellido']) !!}

                  @if ($errors->has('competitors.:next:.name'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.:next:.name') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Name -->

        <div class="visible-sm clearfix"></div>

        <!-- Init Input Email -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.:next:.email') ? ' has-error' : '' }}">
              {!! Form::label('email', trans('label.email'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('competitors[:next:][email]', null,['class' => 'form-control input-sm',  'placeholder'=>trans('label.email')]) !!}

                  @if ($errors->has('competitors.:next:.email'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.:next:.email') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Email -->

        <div class="visible-md visible-lg clearfix"></div>

        <!-- Init Input Gender -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.:next:.gender') ? ' has-error' : '' }}">
              {!! Form::label('gender', trans('label.gender'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::select('competitors[:next:][gender]', ['M' => 'Masculino', 'F' => 'Femenino'], null, ['class' => 'form-control input-sm', 'placeholder' => '-- Selecione --']) !!}

                  @if ($errors->has('competitors.:next:.gender'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.:next:.gender') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div> 
        <!-- End Input Gender -->

        <div class="visible-sm clearfix"></div>

        <!-- Init Input Size -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.:next:.size') ? ' has-error' : '' }}">
              {!! Form::label('size', trans('label.size'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::select('competitors[:next:][size]', array_pluck($sizes,'name','id'), null, ['class' => 'form-control input-sm', 'placeholder' => '-- Selecione --']) !!}

                  @if ($errors->has('competitors.:next:.size'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.:next:.size') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Size -->  

        <!-- Init Input Birthdate -->
        <div class="col-sm-6 col-md-4">

          <div class="form-group{{ $errors->has('competitors.:next:.birthdate') ? ' has-error' : '' }}">
              {!! Form::label('birthdate', trans('label.birthdate'), ['class' => 'col-md-12']) !!}

              <div class="col-md-12">
                {!! Form::text('competitors[:next:][birthdate]', null,['class' => 'form-control input-sm datemask', 'placeholder'=>'dd-mm-yyyy', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'", 'data-mask'=>'dd-mm-yyyy']) !!}

                  @if ($errors->has('competitors.:next:.birthdate'))
                      <span class="help-block">
                          <strong>{{ $errors->first('competitors.:next:.birthdate') }}</strong>
                      </span>
                  @endif
              </div>

          </div>  

        </div>
        <!-- End Input Birthdate --> 

    </div>
    <!-- End block -->

  </div>  
  <!-- End card -->`;
</script>