@extends('layouts.main')

@section('style')
	{!! Html::style('assets/vendors/sweetalert/sweetalert.min.css') !!} 
	<style type="text/css">
		
@media 
	only screen and (max-width: 768px),
	(min-device-width: 768px) and (max-device-width: 1023px)  {

		table, thead, tbody, th, td, tr { 
			display: block; 
		}
	
		table {width:100%;}
		thead {display: none;}		
		td{display: block;  text-align:center;}
		td:before { 
		    content: attr(data-th); 
		    display: block;
		    text-align:center;  
		  }

		table {width:100%;}
		thead {display: none;}		
		/*td{display: inline-flex;  text-align:center; width: 100%;}*/
		td{display: inline-flex;  text-align:center; width: 100%;}
		td:before { 
		    content: attr(data-th)": ";
		    display: block;
		    text-align:center;  
		    font-weight: bold;
		    width: 38%
		  }

	}

	@media 
	only screen and (max-width: 600px),
	(min-device-width: 600px) and (max-device-width: 600px)  {
		td{display: block;  text-align:center; width: 100%;}

		td:before{
			width: 100%;
		}
	}
	</style>
@endsection

@section('content')

<div class="bg-gray m-b-30">
  <div class="container">  
  <div class="col-md-12">
        <ol class="breadcrumb">
          <li><a href="{{route('front.inscription')}}">Inscripción</a></li>
          <li class="active">Detalle</li>
        </ol>
      </div>
  </div>
</div>

<div class="container m-b-30">

        <div class="col-md-12">
        @if (session('danger'))
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong><i class="fa fa-check"></i> {{trans('message.danger')}}</strong> {{ session('danger') }}
            </div>
          @endif
            <div class="panel panel-default">
                <div class="panel-heading f-18"><i class="fa fa-file-text" aria-hidden="true"></i> &nbsp;
 Inscripción: <span class="text-info">{{str_pad($inscription->id,7,"0",STR_PAD_LEFT)}}</span></div>

                <div class="panel-body" style="padding-top: 20px;">
                  <div class="row">
                  
	                <div class="col-xs-12 col-sm-3 col-md-2">
                        @if($inscription->status=='approved')
                    		<a class="btn btn-default" href="{{route('front.inscription.ticket',$inscription->code)}}" target="_blank"><i class="fa fa-print f-20"></i> Comprobante</a>
                    	@endif
                    </div>  
                    <div class="clearfix visible-xs" style="margin-top:40px;"></div>                
	                <div class="col-xs-12 col-sm-6 col-md-8 text-right">
	                   <i class="fa fa-calendar f-20"></i> &nbsp;{{$inscription->created_at_es}}
	                </div>
	                <div class="clearfix visible-xs" style="margin-top:40px;"></div>
	                <div class="col-xs-12 col-sm-3 col-md-2 text-right">
	                   @if($inscription->status=='pending')	
	                     <span class="label label-warning" style="padding: 8px 12px; font-size: 16px;">
	                     	<i class="fa fa-exclamation-circle"></i> {{trans('label.'.$inscription->status)}}
	                     </span> 
	                   @elseif($inscription->status=='approved')
	                   	 <span class="label label-success" style="padding: 8px 12px; font-size: 16px;" >
	                        <i class="fa fa-check"></i>	{{trans('label.'.$inscription->status)}}
	                     </span> 
	                   @elseif($inscription->status=='rejected')
	                   	 <span class="label label-danger" style="padding: 8px 12px; font-size: 16px;">
	                     	<i class="fa fa-ban"></i> {{trans('label.'.$inscription->status)}}
	                     </span> 
	                   @elseif($inscription->status=='process')
	                   	 <span class="label label-primary" style="padding: 8px 12px; font-size: 16px;">
	                     	<i class="fa fa-spinner"></i> {{trans('label.'.$inscription->status)}}
	                     </span>
	                   @endif	                   
	                </div>

	                <div class="col-xs-12 col-sm-12 col-md-12">
	                	<hr>
	                </div>

	                <div class="col-md-12">
	                	<h5 class="f-16 p-l-10"><i class="fa fa-users f-20 text-info"></i> &nbsp;Participantes {{--&nbsp;&nbsp;<i class="fa fa-plus-square f-20 text-info"></i> &nbsp;Cantidad: <span class="">{{count($inscription->competitors)}}</span>--}}</h5>
	                	<table class="table table-striped">
	                	  <thead>
	                	   <tr>
	                		<th width="130" class="text-center">{{trans('label.identification')}}</th>
	                		<th class="text-center">{{trans('label.name')}}</th>
	                		<th class="text-center">{{trans('label.email')}}</th>
	                		<th width="60" class="text-center">{{trans('label.size')}}</th>
	                		<th width="70" class="text-center">{{trans('label.gender')}}</th>
	                		<th width="150" class="text-center">{{trans('label.birthdate')}}</th>
	                		<th width="60" class="text-center">{{trans('label.age')}}</th>
	                	   </tr>
	                	  </thead>
	                	  <tbody>
	                	   @foreach($inscription->competitors as $competitor)
	                	   	<tr>
	                	   	 <td data-th="{{trans('label.identification')}}" class="text-center">{{$competitor->identification}}</td>
	                	   	 <td data-th="{{trans('label.name')}}" class="text-center">{{$competitor->name}}</td>
	                	   	 <td data-th="{{trans('label.email')}}" class="text-center">{{$competitor->email}}</td>
	                	   	 <td data-th="{{trans('label.size')}}" class="text-center">{{$competitor->size->name}}</td>
	                	   	 <td data-th="{{trans('label.gender')}}" class="text-center">{{$competitor->gender}}</td>
	                	   	 <td data-th="{{trans('label.birthdate')}}" class="text-center">{{$competitor->birthdate_es}}</td>
	                	   	 <td data-th="{{trans('label.age')}}" class="text-center">{{$competitor->age}}</td>
	                	   	</tr>
	                	   @endforeach
	                	   
	                	  </tbody>
	                	</table>
	                </div>

	                <div class="col-md-12">
	                	<hr>
	                </div>

	                <div class="col-md-12">
	                	<h5 class="f-16 p-l-10"><i class="fa fa-dollar f-20 text-warning"></i> &nbsp;Pagos {{--&nbsp;&nbsp;<i class="fa fa-money f-20 text-warning"></i> &nbsp;Total: <span class="">{{number_format(collect($inscription->payments)->sum('amount'),'2',',','')}}</span>--}}</h5>
	                	<table class="table table-striped">
	                	  <thead>
	                	   <tr>
	                		<th class="text-center">{{trans('label.payment_method')}}</th>
	                		{{--<th class="text-center">{{trans('label.amount')}}</th>--}}
	                		<th class="text-center">{{trans('label.number')}}</th>
	                		<th class="text-center">{{trans('label.bank')}}</th>	                		
	                	   </tr>
	                	  </thead>
	                	  <tbody>
	                	   @foreach($inscription->payments as $payment)
	                	   <tr>
	                		<td data-th="{{trans('label.payment_method')}}" class="text-center">{{$payment->method}}</td>
	                		{{--<td class="text-center">{{number_format($payment->amount,'2',',','')}}</td>--}}
	                		<td data-th="{{trans('label.number')}}" class="text-center">{{$payment->number ? $payment->number : '' }}</td>
	                		<td data-th="{{trans('label.bank')}}" class="text-center">{{$payment->bank ? $payment->bank->name : ''}}</td> 		
	                	   </tr>
	                	   @endforeach
	                	  </tbody>
	                	</table>

	                </div>

	                <div class="col-md-12">
	                	<hr>
	                </div>	                
                  
                </div>
            </div>
           </div>
        </div>
    </div>
</div>
@include('pages.sponsor')
<div class="p-t-40 p-b-30 question-footer">
  <div class="container">  
    <div class="col-md-8"><h3 class="text-center f-20 text-semibold p-t-m-t-0" >¿Tienes una duda sobre la carrera 5K por la educación?</h3></div> <div class="col-md-4 text-center p-t-20"><a href="{{url('help')}}" class="btn btn-default btn-l text-uppercase"><i class="fa fa-question-circle"></i> Ayuda</a></div>
  </div>
</div>
@endsection

@section('script')
  {!! Html::script('assets/vendors/sweetalert/sweetalert.min.js') !!}    

  @include('sweet::alert')

@endsection