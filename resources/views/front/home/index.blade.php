@extends('layouts.main')

@section('style')
  
@endsection

@section('content')

{{--dd($sliders)--}}

<div class="bg-gray ">
  <div class="container">  
  <div class="col-md-12">
    <ol class="breadcrumb">          
      <li><a href="{{route('front.index')}}">Inicio</a></li>
      <li class="active">Carrera 5K</li>
    </ol>
  </div>
  </div>
</div>


@if(count($sliders)>0)
<div id="myCarousel" class="carousel slide m-b-30">
<div class="carousel-inner">
@foreach($sliders as $slider)
<article class="item cargador @if ($loop->first) active @endif">
<img src="{{asset('assets/file/'.$slider->banner)}}" alt="Banner {{$slider->name}}" title="{{$slider->name}}" class="img-responsive img-slider post-carga width="100%">
{{--<div class="carousel-caption">
<h3>Headline</h3>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
<p><a class="btn btn-info btn-sm">Read More</a></p>
</div>--}}
</article>
@endforeach

</div>
<!-- Indicators -->
<ol class="carousel-indicators">
@php
 $slider_to=0;
@endphp 
@foreach($sliders as $slider)
<li data-target="#myCarousel" data-slide-to="{{$slider_to}}" @if ($loop->first) class="active" @endif></li>
@php
  $slider_to=$slider_to+1;
@endphp
@endforeach
</ol>
<!-- Controls -->
<div class="carousel-controls">
<a class="carousel-control left" href="#myCarousel" data-slide="prev">
<span class="fa fa-angle-left"></span>

</a>
<a class="carousel-control right" href="#myCarousel" data-slide="next">
<span class="fa fa-angle-right"></span>

</a>
</div>
</div>
@else
 <div class="m-b-30"></div>
@endif

<div class="container">

  
  <div class="col-md-12">

   <div class="row">

   <header class="section-header"><span class="f-14">conoce todo sobre la carrera</span> <h2 class="f-20"><i class="fa fa-home f-20"></i> Inicio</h2></header>

    <div class="card">        
        <div class="card-block p-b-30">
            <div class="col-md-12"> 
              <div class="row">
              @if($page)
                {!! $page->content !!}
              @endif
             </div>
            </div>
        </div>
    </div>
       
   </div>

  </div>

</div>
{{--<div style="background-color: #FFFFFF;margin-top: 30px;" class="p-t-40 p-b-30">

	<header class="section-header"><span class="f-14">nuestros aliados</span> <h2 class="f-20"><i class="fa fa-angle-double-right f-20"></i> Patrocinantes</h2></header>

  <div class="clearfix"></div>
  <div class="container">
    <div class="row">
    @php
      $count=1;
    @endphp
    @foreach($sponsors as $sponsor)
      @php
        if($count==4){
          $count=1;
        }
      @endphp

    <div class="col-sm-6 col-md-4 col-lg-3" style="height: 100% !important;" >
      <img src="{{asset('assets/file/'.$sponsor->logo)}}" style="max-width: 270px; margin-bottom: 20px;" alt="Patrocinante {{$sponsor->name}}" class="img-responsive sponsor_official">
    </div>
      @php
        if($count==2){
          echo '<div class="visible-sm clearfix"></div>';
        }elseif($count==3){
          echo '<div class="visible-md clearfix"></div>';
        }
        $count=$count+1;
      @endphp
    @endforeach
   </div>
  </div>

</div>--}}
@include('pages.sponsor')
<div class="p-t-40 p-b-30 question-footer">
      <div class="container">  
        <div class="col-md-8"><h3 class="text-center f-20 text-semibold p-t-m-t-0" >¿Quieres participar en la carrera 5K por la educación?</h3></div> <div class="col-md-4 text-center p-t-20"><a href="{{url('inscription')}}" class="btn btn-default btn-l text-uppercase"><i class="fa fa-user-plus"></i> Inscribirme</a></div>
      </div>
    </div>
    
<!--</div>-->
@endsection

@section('script')
  <script type="text/javascript">
    $('#myCarousel').carousel({
      interval: 4000
    });
  </script>
@endsection