@extends('layouts.main')

@section('style')
  
@endsection

@section('content')
<div class="bg-gray m-b-30">
  <div class="container">  
  <div class="col-md-12">
    <ol class="breadcrumb">          
      <li><a href="{{route('front.index')}}">Inicio</a></li>
      <li class="active">Ayuda</li>
    </ol>
  </div>
  </div>
</div>

<div class="container">

  
  <div class="col-md-12">

   <div class="row">

   <header class="section-header"><span class="f-14">preguntas frecuentes</span> <h2 class="f-20"><i class="fa fa-question-circle f-20"></i> Ayuda</h2></header>

    <div class="card">        
        <div class="card-block p-t-30 p-b-30">
            <div class="col-md-12"> 
              <div class="row">
              @if($page)
                {!! $page->content !!}
              @endif
              </div>
            </div>
        </div>
    </div>
       
   </div>

  </div>

</div>
@include('pages.sponsor')
<div class="p-t-40 p-b-30 question-footer">
      <div class="container">  
        <div class="col-md-8"><h3 class="text-center f-20 text-semibold p-t-m-t-0" >¿Quieres participar en la carrera 5K por la educación?</h3></div> <div class="col-md-4 text-center p-t-20"><a href="{{url('inscription')}}" class="btn btn-default btn-l text-uppercase"><i class="fa fa-user-plus"></i> Inscribirme</a></div>
      </div>
    </div>
    
<!--</div>-->
@endsection