@extends('layouts.main')

@section('style')
  
@endsection

@section('content')
<div class="bg-gray m-b-30">
  <div class="container">  
  <div class="col-md-12">
    <ol class="breadcrumb">          
      <li><a href="{{route('front.index')}}">Inicio</a></li>
      <li class="active">Contacto</li>
    </ol>
  </div>
  </div>
</div>

<div class="container">

  
  <div class="col-md-12">

   <div class="row">

   <header class="section-header"><span class="f-14">cuentanos tus inquietudes y dudas</span> <h2 class="f-20"><i class="fa fa-question-circle f-20"></i> Contacto</h2></header>

    <div class="card">        
        <div class="card-block">
            @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong><i class="fa fa-remove"></i> Error</strong> Verifique los datos introducidos.
            </div>
	        @endif
	        @if (session('success'))
	          <div class="alert alert-success alert-dismissible" role="alert">
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <strong><i class="fa fa-check"></i> {{trans('message.success')}}</strong> {{ session('success') }}
	          </div>
	        @endif
            <div class="col-sm-6 col-md-6">
                {!! Form::open(['route' => 'contact.send','class'=>'form-horizontal']) !!}
                    {{ csrf_field() }}
                    <h4><b>Formulario</b></h4>
                    <div class="p-t-25"></div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', trans('label.name'), ['class' => 'col-md-12']) !!}

                        <div class="col-md-12">
                          {!! Form::text('name', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>'Nombre y apellido']) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('email', trans('label.email'), ['class' => 'col-md-12']) !!}

                        <div class="col-md-12">
                          {!! Form::text('email', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.email')]) !!}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        {!! Form::label('phone', trans('label.phone'), ['class' => 'col-md-12']) !!}

                        <div class="col-md-12">
                          {!! Form::text('phone', null,['class' => 'form-control', 'autofocus'=>'autofocus', 'placeholder'=>trans('label.phone')]) !!}

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                        {!! Form::label('message', trans('label.message'), ['class' => 'col-md-12']) !!}


                        <div class="col-md-12">
                            {!! Form::textarea('message', null,['class'=>'form-control','rows'=>'4', 'placeholder'=>trans('label.message')]) !!}

                            @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12">
                            {!! Form::button(trans('action.send'),['class'=>'pull-right btn btn-success btn-l','type'=>'submit']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
              </div>

              <div class="col-sm-6 col-md-6">
                <h4><b>Información</b></h3>
                <div class="p-t-25"></div>
                <p><i class="fa fa-envelope f-16 p-t-20"></i> <b>Correo:</b> rborges@uejavier.com – bcoronel@uejavier.com</p>
                <hr>
                {{--<p><i class="fa fa-phone f-20 p-t-10 p-b-10"></i> <b>Teléfono Local:</b> 02617315345</p>
                <hr>--}}
                <p><i class="fa fa-mobile f-20 p-t-10 p-b-10"></i> <b>Teléfono Movil:</b> 0958993412 – 0960605500</p>
                {{--<hr>
                <p><i class="fa fa-whatsapp f-18 p-t-10 p-b-10"></i> <b>Whatsapp:</b> +5934263670894</p>--}}
                <hr>
                <p><i class="fa fa-map-marker f-18 p-t-10"></i> <b>Ubicación:</b> Guayaquil - Ecuador</p>
              </div>
        </div>
    </div>
       
   </div>

  </div>

</div>
@include('pages.sponsor')
<div class="p-t-40 p-b-30 question-footer">
      <div class="container">  
        <div class="col-md-8"><h3 class="text-center f-20 text-semibold p-t-m-t-0" >¿Quieres participar en la carrera 5K por la educación?</h3></div> <div class="col-md-4 text-center p-t-20"><a href="{{url('inscription')}}" class="btn btn-default btn-l text-uppercase"><i class="fa fa-user-plus"></i> Inscribirme</a></div>
      </div>
    </div>
    
<!--</div>-->
@endsection