<?php

use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $pages=[
        [
          'type'=>'home',
          'content'=>''
        ],
        [
          'type'=>'help',
          'content'=>''
        ],
        [
          'type'=>'info',
          'content'=>''
        ],
        [
          'type'=>'terms',
          'content'=>''
        ]

      ];

      foreach($pages as $page){
        $new= new App\Page();
        $new->type=$page['type'];
        $new->content=$page['content'];        
        $new->save();
      }

    }
}
