<?php

use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $banks=[
        [
          'name'=>'Banco Litoral'
        ],
        [
          'name'=>'Banco Pacifico'
        ],
        [
          'name'=>'Banco Guayaquil'
        ]
      ];

      foreach($banks as $bank){
        $new= new App\Bank();
        $new->name=$bank['name'];                
        $new->save();
      }

    }
}
