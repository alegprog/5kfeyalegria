<?php

use Illuminate\Database\Seeder;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $sizes=[
        [
          'name'=>'S'
        ],
        [
          'name'=>'M'
        ],
        [
          'name'=>'L'
        ]
      ];

      foreach($sizes as $size){
        $new= new App\Size();
        $new->name=$size['name'];                
        $new->save();
      }

    }
}