<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users=[
        [
          'name'=>'Alejandro Garcia',
          'email'=>'alejandrogarcia15@gmail.com',
          'password'=>'123456',          
          'type'=>'master'
        ],
        [
          'name'=>'Alfonso Garcia',
          'email'=>'aleprog@gmail.com',
          'password'=>'123456',          
          'type'=>'manager'
        ],
        [
          'name'=>'Operador ',
          'email'=>'operador@gmail.com',
          'password'=>'123456',
          'type'=>'operator'
        ]
      ];

      foreach($users as $user){
        $new= new App\User();
        $new->name=$user['name'];
        $new->email=$user['email'];
        $new->password=bcrypt($user['password']);        
        $new->type=$user['type'];
        $new->save();
      }

    }
}
